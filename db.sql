-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table ffa.admin_commission
DROP TABLE IF EXISTS `admin_commission`;
CREATE TABLE IF NOT EXISTS `admin_commission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin` int(11) DEFAULT NULL,
  `balance` decimal(11,2) DEFAULT NULL,
  `regbalance` decimal(11,2) DEFAULT NULL,
  `salemerite` decimal(11,2) DEFAULT NULL,
  `groupBonus` decimal(11,2) DEFAULT NULL,
  `levelBonus` decimal(11,2) DEFAULT NULL,
  `startupbonus` decimal(11,2) DEFAULT NULL,
  `bottles` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_D45BA7DD880E0D76` (`admin`),
  CONSTRAINT `FK_D45BA7DD880E0D76` FOREIGN KEY (`admin`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.admin_commission: ~2 rows (approximately)
/*!40000 ALTER TABLE `admin_commission` DISABLE KEYS */;
INSERT INTO `admin_commission` (`id`, `admin`, `balance`, `regbalance`, `salemerite`, `groupBonus`, `levelBonus`, `startupbonus`, `bottles`, `created`) VALUES
	(1, 2, NULL, 260.00, NULL, NULL, NULL, NULL, NULL, '2019-12-13 04:18:14'),
	(2, 3, 0.00, 0.00, NULL, 0.00, NULL, NULL, NULL, '2019-12-13 17:35:42');
/*!40000 ALTER TABLE `admin_commission` ENABLE KEYS */;

-- Dumping structure for table ffa.branches
DROP TABLE IF EXISTS `branches`;
CREATE TABLE IF NOT EXISTS `branches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `names` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `joinside` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leftTotal` int(11) DEFAULT NULL,
  `rightTotal` int(11) DEFAULT NULL,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `follwers` int(11) NOT NULL,
  `rBranch` int(11) DEFAULT NULL,
  `lBranch` int(11) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `IDX_D760D16FF132696E` (`userid`),
  KEY `IDX_D760D16FEB73A1F6` (`rBranch`),
  KEY `IDX_D760D16FDC924665` (`lBranch`),
  CONSTRAINT `FK_D760D16FDC924665` FOREIGN KEY (`lBranch`) REFERENCES `client_code` (`id`),
  CONSTRAINT `FK_D760D16FEB73A1F6` FOREIGN KEY (`rBranch`) REFERENCES `client_code` (`id`),
  CONSTRAINT `FK_D760D16FF132696E` FOREIGN KEY (`userid`) REFERENCES `client_code` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.branches: ~0 rows (approximately)
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;

-- Dumping structure for table ffa.clients
DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) DEFAULT NULL,
  `clientcommission` int(11) DEFAULT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `agreed` tinyint(1) DEFAULT NULL,
  `piecenumber` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dateofbirth` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C82E74BAD9704A` (`clientcommission`),
  KEY `IDX_C82E74F92F3E70` (`country_id`),
  CONSTRAINT `FK_C82E74BAD9704A` FOREIGN KEY (`clientcommission`) REFERENCES `client_commission` (`id`),
  CONSTRAINT `FK_C82E74F92F3E70` FOREIGN KEY (`country_id`) REFERENCES `pays` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.clients: ~1 rows (approximately)
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
INSERT INTO `clients` (`id`, `country_id`, `clientcommission`, `firstname`, `lastname`, `gender`, `email`, `telephone`, `address`, `created`, `agreed`, `piecenumber`, `dateofbirth`) VALUES
	(1, 2, NULL, 'Gaston', 'Claude', '1', 'radjimubarak@gmial.com', '12948', NULL, '2019-12-13 16:19:32', NULL, NULL, NULL);
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;

-- Dumping structure for table ffa.client_code
DROP TABLE IF EXISTS `client_code`;
CREATE TABLE IF NOT EXISTS `client_code` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `activatedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.client_code: ~3 rows (approximately)
/*!40000 ALTER TABLE `client_code` DISABLE KEYS */;
INSERT INTO `client_code` (`id`, `code`, `status`, `created`, `activatedDate`) VALUES
	(1, 'FHL7036982', 0, '2019-12-13 16:18:25', '2019-12-13 16:18:25'),
	(2, 'FHL4601935', 1, '2019-12-13 16:19:32', '2019-12-13 16:19:32'),
	(3, 'GP1365729', 0, '2019-12-13 17:35:41', NULL);
/*!40000 ALTER TABLE `client_code` ENABLE KEYS */;

-- Dumping structure for table ffa.client_commission
DROP TABLE IF EXISTS `client_commission`;
CREATE TABLE IF NOT EXISTS `client_commission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client` int(11) DEFAULT NULL,
  `levelCommission` decimal(10,2) NOT NULL,
  `bottles` int(11) DEFAULT NULL,
  `groupBonusAllowed` tinyint(1) DEFAULT NULL,
  `salesBonusAllowed` tinyint(1) DEFAULT NULL,
  `balance` decimal(11,2) DEFAULT NULL,
  `levelMerit` decimal(11,2) DEFAULT NULL,
  `levelaward` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `levelLogistic` decimal(11,2) DEFAULT NULL,
  `groupBonus` decimal(11,2) DEFAULT NULL,
  `regbalance` decimal(11,2) DEFAULT NULL,
  `salemerite` decimal(11,2) DEFAULT NULL,
  `salelogistics` decimal(11,2) DEFAULT NULL,
  `salesaward` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv` decimal(11,2) DEFAULT NULL,
  `mypv` decimal(11,2) DEFAULT NULL,
  `newpv` decimal(11,2) DEFAULT NULL,
  `tempPv` decimal(11,2) DEFAULT NULL,
  `startupbonus` decimal(11,2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8C313C19C7440455` (`client`),
  CONSTRAINT `FK_8C313C19C7440455` FOREIGN KEY (`client`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.client_commission: ~1 rows (approximately)
/*!40000 ALTER TABLE `client_commission` DISABLE KEYS */;
INSERT INTO `client_commission` (`id`, `client`, `levelCommission`, `bottles`, `groupBonusAllowed`, `salesBonusAllowed`, `balance`, `levelMerit`, `levelaward`, `levelLogistic`, `groupBonus`, `regbalance`, `salemerite`, `salelogistics`, `salesaward`, `pv`, `mypv`, `newpv`, `tempPv`, `startupbonus`, `created`) VALUES
	(1, 1, 0.00, NULL, 0, 1, 0.00, NULL, NULL, NULL, NULL, 999740.00, NULL, 0.00, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, '2019-12-13 16:19:32');
/*!40000 ALTER TABLE `client_commission` ENABLE KEYS */;

-- Dumping structure for table ffa.commissions
DROP TABLE IF EXISTS `commissions`;
CREATE TABLE IF NOT EXISTS `commissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(10,2) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.commissions: ~9 rows (approximately)
/*!40000 ALTER TABLE `commissions` DISABLE KEYS */;
INSERT INTO `commissions` (`id`, `level`, `value`, `created`) VALUES
	(1, '1', 4.20, '2018-10-22 22:31:38'),
	(2, '2', 29.40, '2018-10-22 22:37:34'),
	(3, '3', 196.00, '2018-10-22 22:37:57'),
	(4, '4', 1078.00, '2018-10-22 22:46:12'),
	(5, '5', 4900.00, '2018-10-22 22:47:03'),
	(6, '6', 15680.00, '2018-10-22 22:47:24'),
	(7, '7', 78400.00, '2018-10-22 22:47:46'),
	(8, '8', 392000.00, '2018-10-22 22:48:14'),
	(9, 'INFINITY', 0.00, '2018-11-08 00:00:00');
/*!40000 ALTER TABLE `commissions` ENABLE KEYS */;

-- Dumping structure for table ffa.editables
DROP TABLE IF EXISTS `editables`;
CREATE TABLE IF NOT EXISTS `editables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `editField` longtext COLLATE utf8mb4_unicode_ci,
  `lastModified` datetime DEFAULT NULL,
  `modifiedBy` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8FF7D4B0ECF9771D` (`modifiedBy`),
  CONSTRAINT `FK_8FF7D4B0ECF9771D` FOREIGN KEY (`modifiedBy`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.editables: ~0 rows (approximately)
/*!40000 ALTER TABLE `editables` DISABLE KEYS */;
/*!40000 ALTER TABLE `editables` ENABLE KEYS */;

-- Dumping structure for table ffa.level_awards
DROP TABLE IF EXISTS `level_awards`;
CREATE TABLE IF NOT EXISTS `level_awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `award` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.level_awards: ~0 rows (approximately)
/*!40000 ALTER TABLE `level_awards` DISABLE KEYS */;
/*!40000 ALTER TABLE `level_awards` ENABLE KEYS */;

-- Dumping structure for table ffa.migration_versions
DROP TABLE IF EXISTS `migration_versions`;
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.migration_versions: ~0 rows (approximately)
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` (`version`, `executed_at`) VALUES
	('20191124184834', '2019-11-24 18:48:56'),
	('20191213152213', '2019-12-13 15:22:23');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Dumping structure for table ffa.notifications
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `startdate` date DEFAULT NULL,
  `enddate` date DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.notifications: ~2 rows (approximately)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` (`id`, `object`, `description`, `startdate`, `enddate`, `created`) VALUES
	(1, 'Test', '16-12-2019', '2019-12-16', '2020-01-03', '2019-12-13 11:03:53');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Dumping structure for table ffa.orders
DROP TABLE IF EXISTS `orders`;
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `ordernumber` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `productnumber` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `registeredUser` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_E52FFDEE8D93D649` (`user`),
  KEY `IDX_E52FFDEED59660C8` (`registeredUser`),
  CONSTRAINT `FK_E52FFDEE8D93D649` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_E52FFDEED59660C8` FOREIGN KEY (`registeredUser`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.orders: ~21 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `user`, `ordernumber`, `productnumber`, `type`, `created`, `registeredUser`) VALUES
	(1, 1, 'HL1001', NULL, 'Registration', '2019-12-14 07:46:57', 8),
	(2, 1, 'HL1002', NULL, 'Registration', '2019-12-14 08:02:29', 9),
	(3, 1, 'HL1003', NULL, 'Registration', '2019-12-14 08:17:52', 10),
	(4, 1, 'HL1004', NULL, 'Registration', '2019-12-14 08:23:08', 11),
	(5, 1, 'HL1005', NULL, 'Registration', '2019-12-14 08:27:18', 12),
	(6, 1, 'HL1006', NULL, 'Registration', '2019-12-14 08:45:28', 13),
	(7, 1, 'HL1007', NULL, 'Registration', '2019-12-14 08:46:11', 14),
	(8, 1, 'HL1008', NULL, 'Registration', '2019-12-14 09:29:51', 15),
	(9, 1, 'HL1009', NULL, 'Registration', '2019-12-14 09:32:25', 16),
	(10, 1, 'HL10010', NULL, 'Registration', '2019-12-14 09:33:15', 17),
	(11, 1, 'HL10011', NULL, 'Registration', '2019-12-14 09:42:04', 18),
	(12, 1, 'HL10012', NULL, 'Registration', '2019-12-14 09:42:57', 19),
	(13, 1, 'HL10013', NULL, 'Registration', '2019-12-14 09:59:57', 20),
	(14, 1, 'HL10014', NULL, 'Registration', '2019-12-14 10:02:28', 21),
	(15, 1, 'HL10015', NULL, 'Registration', '2019-12-15 08:50:20', 22),
	(16, 1, 'HL10016', NULL, 'Registration', '2019-12-15 08:57:56', 4),
	(17, 1, 'HL10017', NULL, 'Registration', '2019-12-15 09:01:20', 5),
	(18, 1, 'HL10018', NULL, 'Registration', '2019-12-15 09:02:27', 6),
	(19, 1, 'HL10019', NULL, 'Registration', '2019-12-15 09:11:24', 7),
	(20, 1, 'HL10020', NULL, 'Registration', '2019-12-15 09:12:10', 8),
	(21, 1, 'HL10021', NULL, 'Registration', '2019-12-15 09:14:17', 9),
	(22, 1, 'HL10022', NULL, 'Registration', '2019-12-15 09:15:06', 10);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table ffa.pays
DROP TABLE IF EXISTS `pays`;
CREATE TABLE IF NOT EXISTS `pays` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=239 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.pays: ~238 rows (approximately)
/*!40000 ALTER TABLE `pays` DISABLE KEYS */;
INSERT INTO `pays` (`id`, `code`, `fr`, `en`) VALUES
	(1, 'AF', 'Afghanistan', 'Afghanistan'),
	(2, 'ZA', 'Afrique du Sud', 'South Africa'),
	(3, 'AL', 'Albanie', 'Albania'),
	(4, 'DZ', 'Algérie', 'Algeria'),
	(5, 'DE', 'Allemagne', 'Germany'),
	(6, 'AD', 'Andorre', 'Andorra'),
	(7, 'AO', 'Angola', 'Angola'),
	(8, 'AI', 'Anguilla', 'Anguilla'),
	(9, 'AQ', 'Antarctique', 'Antarctica'),
	(10, 'AG', 'Antigua-et-Barbuda', 'Antigua & Barbuda'),
	(11, 'AN', 'Antilles néerlandaises', 'Netherlands Antilles'),
	(12, 'SA', 'Arabie saoudite', 'Saudi Arabia'),
	(13, 'AR', 'Argentine', 'Argentina'),
	(14, 'AM', 'Arménie', 'Armenia'),
	(15, 'AW', 'Aruba', 'Aruba'),
	(16, 'AU', 'Australie', 'Australia'),
	(17, 'AT', 'Autriche', 'Austria'),
	(18, 'AZ', 'Azerbaïdjan', 'Azerbaijan'),
	(19, 'BJ', 'Bénin', 'Benin'),
	(20, 'BS', 'Bahamas', 'Bahamas, The'),
	(21, 'BH', 'Bahreïn', 'Bahrain'),
	(22, 'BD', 'Bangladesh', 'Bangladesh'),
	(23, 'BB', 'Barbade', 'Barbados'),
	(24, 'PW', 'Belau', 'Palau'),
	(25, 'BE', 'Belgique', 'Belgium'),
	(26, 'BZ', 'Belize', 'Belize'),
	(27, 'BM', 'Bermudes', 'Bermuda'),
	(28, 'BT', 'Bhoutan', 'Bhutan'),
	(29, 'BY', 'Biélorussie', 'Belarus'),
	(30, 'MM', 'Birmanie', 'Myanmar (ex-Burma)'),
	(31, 'BO', 'Bolivie', 'Bolivia'),
	(32, 'BA', 'Bosnie-Herzégovine', 'Bosnia and Herzegovina'),
	(33, 'BW', 'Botswana', 'Botswana'),
	(34, 'BR', 'Brésil', 'Brazil'),
	(35, 'BN', 'Brunei', 'Brunei Darussalam'),
	(36, 'BG', 'Bulgarie', 'Bulgaria'),
	(37, 'BF', 'Burkina Faso', 'Burkina Faso'),
	(38, 'BI', 'Burundi', 'Burundi'),
	(39, 'CI', 'Côte d\'Ivoire', 'Ivory Coast (see Cote d\'Ivoire)'),
	(40, 'KH', 'Cambodge', 'Cambodia'),
	(41, 'CM', 'Cameroun', 'Cameroon'),
	(42, 'CA', 'Canada', 'Canada'),
	(43, 'CV', 'Cap-Vert', 'Cape Verde'),
	(44, 'CL', 'Chili', 'Chile'),
	(45, 'CN', 'Chine', 'China'),
	(46, 'CY', 'Chypre', 'Cyprus'),
	(47, 'CO', 'Colombie', 'Colombia'),
	(48, 'KM', 'Comores', 'Comoros'),
	(49, 'CG', 'Congo', 'Congo'),
	(50, 'KP', 'Corée du Nord', 'Korea, Demo. People\'s Rep. of'),
	(51, 'KR', 'Corée du Sud', 'Korea, (South) Republic of'),
	(52, 'CR', 'Costa Rica', 'Costa Rica'),
	(53, 'HR', 'Croatie', 'Croatia'),
	(54, 'CU', 'Cuba', 'Cuba'),
	(55, 'DK', 'Danemark', 'Denmark'),
	(56, 'DJ', 'Djibouti', 'Djibouti'),
	(57, 'DM', 'Dominique', 'Dominica'),
	(58, 'EG', 'Égypte', 'Egypt'),
	(59, 'AE', 'Émirats arabes unis', 'United Arab Emirates'),
	(60, 'EC', 'Équateur', 'Ecuador'),
	(61, 'ER', 'Érythrée', 'Eritrea'),
	(62, 'ES', 'Espagne', 'Spain'),
	(63, 'EE', 'Estonie', 'Estonia'),
	(64, 'US', 'États-Unis', 'United States'),
	(65, 'ET', 'Éthiopie', 'Ethiopia'),
	(66, 'FI', 'Finlande', 'Finland'),
	(67, 'FR', 'France', 'France'),
	(68, 'GE', 'Géorgie', 'Georgia'),
	(69, 'GA', 'Gabon', 'Gabon'),
	(70, 'GM', 'Gambie', 'Gambia, the'),
	(71, 'GH', 'Ghana', 'Ghana'),
	(72, 'GI', 'Gibraltar', 'Gibraltar'),
	(73, 'GR', 'Grèce', 'Greece'),
	(74, 'GD', 'Grenade', 'Grenada'),
	(75, 'GL', 'Groenland', 'Greenland'),
	(76, 'GP', 'Guadeloupe', 'Guinea, Equatorial'),
	(77, 'GU', 'Guam', 'Guam'),
	(78, 'GT', 'Guatemala', 'Guatemala'),
	(79, 'GN', 'Guinée', 'Guinea'),
	(80, 'GQ', 'Guinée Ã©quatoriale', 'Equatorial Guinea'),
	(81, 'GW', 'Guinée-Bissao', 'Guinea-Bissau'),
	(82, 'GY', 'Guyana', 'Guyana'),
	(83, 'GF', 'Guyane franÃ§aise', 'Guiana, French'),
	(84, 'HT', 'Haïti', 'Haiti'),
	(85, 'HN', 'Honduras', 'Honduras'),
	(86, 'HK', 'Hong Kong', 'Hong Kong, (China)'),
	(87, 'HU', 'Hongrie', 'Hungary'),
	(88, 'BV', 'Ile Bouvet', 'Bouvet Island'),
	(89, 'CX', 'Ile Christmas', 'Christmas Island'),
	(90, 'NF', 'Ile Norfolk', 'Norfolk Island'),
	(91, 'KY', 'Iles Cayman', 'Cayman Islands'),
	(92, 'CK', 'Iles Cook', 'Cook Islands'),
	(93, 'FO', 'Iles Féroé', 'Faroe Islands'),
	(94, 'FK', 'Iles Falkland', 'Falkland Islands (Malvinas)'),
	(95, 'FJ', 'Iles Fidji', 'Fiji'),
	(96, 'GS', 'Iles Géorgie du Sud et Sandwich du Sud', 'S. Georgia and S. Sandwich Is.'),
	(97, 'HM', 'Iles Heard et McDonald', 'Heard and McDonald Islands'),
	(98, 'MH', 'Iles Marshall', 'Marshall Islands'),
	(99, 'PN', 'Iles Pitcairn', 'Pitcairn Island'),
	(100, 'SB', 'Iles Salomon', 'Solomon Islands'),
	(101, 'SJ', 'Iles Svalbard et Jan Mayen', 'Svalbard and Jan Mayen Islands'),
	(102, 'TC', 'Iles Turks-et-Caicos', 'Turks and Caicos Islands'),
	(103, 'VI', 'Iles Vierges américaines', 'Virgin Islands, U.S.'),
	(104, 'VG', 'Iles Vierges britanniques', 'Virgin Islands, British'),
	(105, 'CC', 'Iles des Cocos (Keeling)', 'Cocos (Keeling) Islands'),
	(106, 'UM', 'Iles mineures éloignées des États-Unis', 'US Minor Outlying Islands'),
	(107, 'IN', 'Inde', 'India'),
	(108, 'ID', 'Indonésie', 'Indonesia'),
	(109, 'IR', 'Iran', 'Iran, Islamic Republic of'),
	(110, 'IQ', 'Iraq', 'Iraq'),
	(111, 'IE', 'Irlande', 'Ireland'),
	(112, 'IS', 'Islande', 'Iceland'),
	(113, 'IL', 'Israël', 'Israel'),
	(114, 'IT', 'Italie', 'Italy'),
	(115, 'JM', 'Jamaïque', 'Jamaica'),
	(116, 'JP', 'Japon', 'Japan'),
	(117, 'JO', 'Jordanie', 'Jordan'),
	(118, 'KZ', 'Kazakhstan', 'Kazakhstan'),
	(119, 'KE', 'Kenya', 'Kenya'),
	(120, 'KG', 'Kirghizistan', 'Kyrgyzstan'),
	(121, 'KI', 'Kiribati', 'Kiribati'),
	(122, 'KW', 'Koweït', 'Kuwait'),
	(123, 'LA', 'Laos', 'Lao People\'s Democratic Republic'),
	(124, 'LS', 'Lesotho', 'Lesotho'),
	(125, 'LV', 'Lettonie', 'Latvia'),
	(126, 'LB', 'Liban', 'Lebanon'),
	(127, 'LR', 'Liberia', 'Liberia'),
	(128, 'LY', 'Libye', 'Libyan Arab Jamahiriya'),
	(129, 'LI', 'Liechtenstein', 'Liechtenstein'),
	(130, 'LT', 'Lituanie', 'Lithuania'),
	(131, 'LU', 'Luxembourg', 'Luxembourg'),
	(132, 'MO', 'Macao', 'Macao, (China)'),
	(133, 'MG', 'Madagascar', 'Madagascar'),
	(134, 'MY', 'Malaisie', 'Malaysia'),
	(135, 'MW', 'Malawi', 'Malawi'),
	(136, 'MV', 'Maldives', 'Maldives'),
	(137, 'ML', 'Mali', 'Mali'),
	(138, 'MT', 'Malte', 'Malta'),
	(139, 'MP', 'Mariannes du Nord', 'Northern Mariana Islands'),
	(140, 'MA', 'Maroc', 'Morocco'),
	(141, 'MQ', 'Martinique', 'Martinique'),
	(142, 'MU', 'Maurice', 'Mauritius'),
	(143, 'MR', 'Mauritanie', 'Mauritania'),
	(144, 'YT', 'Mayotte', 'Mayotte'),
	(145, 'MX', 'Mexique', 'Mexico'),
	(146, 'FM', 'Micronésie', 'Micronesia, Federated States of'),
	(147, 'MD', 'Moldavie', 'Moldova, Republic of'),
	(148, 'MC', 'Monaco', 'Monaco'),
	(149, 'MN', 'Mongolie', 'Mongolia'),
	(150, 'MS', 'Montserrat', 'Montserrat'),
	(151, 'MZ', 'Mozambique', 'Mozambique'),
	(152, 'NP', 'Népal', 'Nepal'),
	(153, 'NA', 'Namibie', 'Namibia'),
	(154, 'NR', 'Nauru', 'Nauru'),
	(155, 'NI', 'Nicaragua', 'Nicaragua'),
	(156, 'NE', 'Niger', 'Niger'),
	(157, 'NG', 'Nigeria', 'Nigeria'),
	(158, 'NU', 'Nioué', 'Niue'),
	(159, 'NO', 'Norvège', 'Norway'),
	(160, 'NC', 'Nouvelle-Calédonie', 'New Caledonia'),
	(161, 'NZ', 'Nouvelle-Zélande', 'New Zealand'),
	(162, 'OM', 'Oman', 'Oman'),
	(163, 'UG', 'Ouganda', 'Uganda'),
	(164, 'UZ', 'Ouzbékistan', 'Uzbekistan'),
	(165, 'PE', 'Pérou', 'Peru'),
	(166, 'PK', 'Pakistan', 'Pakistan'),
	(167, 'PA', 'Panama', 'Panama'),
	(168, 'PG', 'Papouasie-Nouvelle-Guinée', 'Papua New Guinea'),
	(169, 'PY', 'Paraguay', 'Paraguay'),
	(170, 'NL', 'pays-Bas', 'Netherlands'),
	(171, 'PH', 'Philippines', 'Philippines'),
	(172, 'PL', 'Pologne', 'Poland'),
	(173, 'PF', 'Polynésie française', 'French Polynesia'),
	(174, 'PR', 'Porto Rico', 'Puerto Rico'),
	(175, 'PT', 'Portugal', 'Portugal'),
	(176, 'QA', 'Qatar', 'Qatar'),
	(177, 'CF', 'République centrafricaine', 'Central African Republic'),
	(178, 'CD', 'République démocratique du Congo', 'Congo, Democratic Rep. of the'),
	(179, 'DO', 'République dominicaine', 'Dominican Republic'),
	(180, 'CZ', 'République tchèque', 'Czech Republic'),
	(181, 'RE', 'Réunion', 'Reunion'),
	(182, 'RO', 'Roumanie', 'Romania'),
	(183, 'GB', 'Royaume-Uni', 'Saint Pierre and Miquelon'),
	(184, 'RU', 'Russie', 'Russia (Russian Federation)'),
	(185, 'RW', 'Rwanda', 'Rwanda'),
	(186, 'SN', 'Sénégal', 'Senegal'),
	(187, 'EH', 'Sahara occidental', 'Western Sahara'),
	(188, 'KN', 'Saint-Christophe-et-Niévès', 'Saint Kitts and Nevis'),
	(189, 'SM', 'Saint-Marin', 'San Marino'),
	(190, 'PM', 'Saint-Pierre-et-Miquelon', 'Saint Pierre and Miquelon'),
	(191, 'VA', 'Saint-Siège ', 'Vatican City State (Holy See)'),
	(192, 'VC', 'Saint-Vincent-et-les-Grenadines', 'Saint Vincent and the Grenadines'),
	(193, 'SH', 'Sainte-Hélène', 'Saint Helena'),
	(194, 'LC', 'Sainte-Lucie', 'Saint Lucia'),
	(195, 'SV', 'Salvador', 'El Salvador'),
	(196, 'WS', 'Samoa', 'Samoa'),
	(197, 'AS', 'Samoa américaines', 'American Samoa'),
	(198, 'ST', 'Sao Tomé-et-Principe', 'Sao Tome and Principe'),
	(199, 'SC', 'Seychelles', 'Seychelles'),
	(200, 'SL', 'Sierra Leone', 'Sierra Leone'),
	(201, 'SG', 'Singapour', 'Singapore'),
	(202, 'SI', 'Slovénie', 'Slovenia'),
	(203, 'SK', 'Slovaquie', 'Slovakia'),
	(204, 'SO', 'Somalie', 'Somalia'),
	(205, 'SD', 'Soudan', 'Sudan'),
	(206, 'LK', 'Sri Lanka', 'Sri Lanka (ex-Ceilan)'),
	(207, 'SE', 'Suède', 'Sweden'),
	(208, 'CH', 'Suisse', 'Switzerland'),
	(209, 'SR', 'Suriname', 'Suriname'),
	(210, 'SZ', 'Swaziland', 'Swaziland'),
	(211, 'SY', 'Syrie', 'Syrian Arab Republic'),
	(212, 'TW', 'Taïwan', 'Taiwan'),
	(213, 'TJ', 'Tadjikistan', 'Tajikistan'),
	(214, 'TZ', 'Tanzanie', 'Tanzania, United Republic of'),
	(215, 'TD', 'Tchad', 'Chad'),
	(216, 'TF', 'Terres australes franÃ§aises', 'French Southern Territories - TF'),
	(217, 'IO', 'Territoire britannique de l\'OcÃ©an Indien', 'British Indian Ocean Territory'),
	(218, 'TH', 'Thaïlande', 'Thailand'),
	(219, 'TL', 'Timor Oriental', 'Timor-Leste (East Timor)'),
	(220, 'TG', 'Togo', 'Togo'),
	(221, 'TK', 'Tokélaou', 'Tokelau'),
	(222, 'TO', 'Tonga', 'Tonga'),
	(223, 'TT', 'Trinité-et-Tobago', 'Trinidad & Tobago'),
	(224, 'TN', 'Tunisie', 'Tunisia'),
	(225, 'TM', 'Turkménistan', 'Turkmenistan'),
	(226, 'TR', 'Turquie', 'Turkey'),
	(227, 'TV', 'Tuvalu', 'Tuvalu'),
	(228, 'UA', 'Ukraine', 'Ukraine'),
	(229, 'UY', 'Uruguay', 'Uruguay'),
	(230, 'VU', 'Vanuatu', 'Vanuatu'),
	(231, 'VE', 'Venezuela', 'Venezuela'),
	(232, 'VN', 'Viêt Nam', 'Viet Nam'),
	(233, 'WF', 'Wallis-et-Futuna', 'Wallis and Futuna'),
	(234, 'YE', 'Yémen', 'Yemen'),
	(235, 'YU', 'Yougoslavie', 'Saint Pierre and Miquelon'),
	(236, 'ZM', 'Zambie', 'Zambia'),
	(237, 'ZW', 'Zimbabwe', 'Zimbabwe'),
	(238, 'MK', 'ex-République yougoslave de Macédoine', 'Macedonia, TFYR');
/*!40000 ALTER TABLE `pays` ENABLE KEYS */;

-- Dumping structure for table ffa.ref_table
DROP TABLE IF EXISTS `ref_table`;
CREATE TABLE IF NOT EXISTS `ref_table` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.ref_table: ~0 rows (approximately)
/*!40000 ALTER TABLE `ref_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `ref_table` ENABLE KEYS */;

-- Dumping structure for table ffa.sales_awards
DROP TABLE IF EXISTS `sales_awards`;
CREATE TABLE IF NOT EXISTS `sales_awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `awards` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.sales_awards: ~0 rows (approximately)
/*!40000 ALTER TABLE `sales_awards` DISABLE KEYS */;
/*!40000 ALTER TABLE `sales_awards` ENABLE KEYS */;

-- Dumping structure for table ffa.transaction
DROP TABLE IF EXISTS `transaction`;
CREATE TABLE IF NOT EXISTS `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) DEFAULT NULL,
  `receiver` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `particulars` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `transferid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transactionid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `transactiontext` longtext COLLATE utf8mb4_unicode_ci,
  `paid` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_723705D15F004ACF` (`sender`),
  KEY `IDX_723705D13DB88C96` (`receiver`),
  CONSTRAINT `FK_723705D13DB88C96` FOREIGN KEY (`receiver`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_723705D15F004ACF` FOREIGN KEY (`sender`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.transaction: ~20 rows (approximately)
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`id`, `sender`, `receiver`, `created`, `particulars`, `amount`, `transferid`, `transactionid`, `type`, `transactiontext`, `paid`) VALUES
	(1, 1, 2, '2019-12-14 07:46:56', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiB ) 14/12/2019 at exactly 07:46:56', 10.00, '191cb381', '389ccc07175', 2, NULL, NULL),
	(2, 1, 2, '2019-12-14 08:02:29', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiB ) 14/12/2019 at exactly 08:02:28', 10.00, '4c9935bd', '8366f27b7b2', 2, NULL, NULL),
	(3, 1, 2, '2019-12-14 08:17:51', '10$ was deducted\n            from your registration commission on for the creation of a new member ( SamuelW ) 14/12/2019 at exactly 08:17:51', 10.00, 'f8439d9e', '9ffcc5342a6', 2, NULL, NULL),
	(4, 1, 2, '2019-12-14 08:23:07', '10$ was deducted\n            from your registration commission on for the creation of a new member ( WillamB ) 14/12/2019 at exactly 08:23:07', 10.00, '9b58b266', '163b5529269', 2, NULL, NULL),
	(5, 1, 2, '2019-12-14 08:27:17', '10$ was deducted\n            from your registration commission on for the creation of a new member ( AdeyemoL ) 14/12/2019 at exactly 08:27:17', 10.00, 'e7e5389e', 'fe119418c9b', 2, NULL, NULL),
	(6, 1, 2, '2019-12-14 08:45:28', '10$ was deducted\n            from your registration commission on for the creation of a new member ( AdeyemoL ) 14/12/2019 at exactly 08:45:28', 10.00, '401f5942', 'e8155e8d43f', 2, NULL, NULL),
	(7, 1, 2, '2019-12-14 08:46:11', '10$ was deducted\n            from your registration commission on for the creation of a new member ( GastonV ) 14/12/2019 at exactly 08:46:11', 10.00, 'bbd51901', 'eca5e8fab80', 2, NULL, NULL),
	(8, 1, 2, '2019-12-14 09:29:51', '10$ was deducted\n            from your registration commission on for the creation of a new member ( GastonV ) 14/12/2019 at exactly 09:29:50', 10.00, '5ace52d3', '7009a23ffaf', 2, NULL, NULL),
	(9, 1, 2, '2019-12-14 09:32:25', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiB ) 14/12/2019 at exactly 09:32:25', 10.00, '01abfaad', 'c8ee7ce5023', 2, NULL, NULL),
	(10, 1, 2, '2019-12-14 09:33:15', '10$ was deducted\n            from your registration commission on for the creation of a new member ( SamuelW ) 14/12/2019 at exactly 09:33:15', 10.00, 'c31062c3', '46cbe20940f', 2, NULL, NULL),
	(11, 1, 2, '2019-12-14 09:42:03', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiB ) 14/12/2019 at exactly 09:42:03', 10.00, 'f2781d9c', '2f759cd7c85', 2, NULL, NULL),
	(12, 1, 2, '2019-12-14 09:42:56', '10$ was deducted\n            from your registration commission on for the creation of a new member ( AdeyemoL ) 14/12/2019 at exactly 09:42:56', 10.00, '391caa2b', 'b1ef0110624', 2, NULL, NULL),
	(13, 1, 2, '2019-12-14 09:59:57', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiB ) 14/12/2019 at exactly 09:59:56', 10.00, '083a57e6', 'ea3e5547df0', 2, NULL, NULL),
	(14, 1, 2, '2019-12-14 10:02:28', '10$ was deducted\n            from your registration commission on for the creation of a new member ( AdeyemoL ) 14/12/2019 at exactly 10:02:28', 10.00, '8d595db8', 'bae3177d4aa', 2, NULL, NULL),
	(15, 1, 2, '2019-12-15 08:50:18', '10$ was deducted\n            from your registration commission on for the creation of a new member ( WillamB ) 15/12/2019 at exactly 08:50:18', 10.00, 'a8d99849', '04e4f7662f2', 2, NULL, NULL),
	(16, 1, 2, '2019-12-15 08:57:55', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiN ) 15/12/2019 at exactly 08:57:55', 10.00, 'c96a7743', '24e0ffa7ba0', 2, NULL, NULL),
	(18, 1, 2, '2019-12-15 09:01:19', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiB ) 15/12/2019 at exactly 09:01:19', 10.00, '158682a2', '984421e635e', 2, NULL, NULL),
	(19, 1, 2, '2019-12-15 09:02:26', '10$ was deducted\n            from your registration commission on for the creation of a new member ( AdeyemoL ) 15/12/2019 at exactly 09:02:26', 10.00, '33a12fc2', 'da56bd84cb9', 2, NULL, NULL),
	(20, 1, 2, '2019-12-15 09:11:22', '10$ was deducted\n            from your registration commission on for the creation of a new member ( SamuelW ) 15/12/2019 at exactly 09:11:22', 10.00, '8e165f3b', 'c3620c4caf0', 2, NULL, NULL),
	(21, 1, 2, '2019-12-15 09:12:09', '10$ was deducted\n            from your registration commission on for the creation of a new member ( GastonV ) 15/12/2019 at exactly 09:12:09', 10.00, 'a8108ba6', '5f09e83f8ce', 2, NULL, NULL),
	(22, 1, 2, '2019-12-15 09:14:16', '10$ was deducted\n            from your registration commission on for the creation of a new member ( RadjiD ) 15/12/2019 at exactly 09:14:16', 10.00, '33902d78', '054a702cbee', 2, NULL, NULL),
	(23, 1, 2, '2019-12-15 09:15:06', '10$ was deducted\n            from your registration commission on for the creation of a new member ( WillamL ) 15/12/2019 at exactly 09:15:06', 10.00, 'da0f4068', '8f40c56a350', 2, NULL, NULL);
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;

-- Dumping structure for table ffa.trans_password
DROP TABLE IF EXISTS `trans_password`;
CREATE TABLE IF NOT EXISTS `trans_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_A9F0388F8D93D649` (`user`),
  CONSTRAINT `FK_A9F0388F8D93D649` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.trans_password: ~0 rows (approximately)
/*!40000 ALTER TABLE `trans_password` DISABLE KEYS */;
INSERT INTO `trans_password` (`id`, `user`, `password`, `created`) VALUES
	(1, 1, '$2y$10$x9WkRRZLwYJTXdKdIPt7yeq4CV1w/XzxTEMtjQe.7viw5DSfItZNu', '2019-12-13 16:38:57');
/*!40000 ALTER TABLE `trans_password` ENABLE KEYS */;

-- Dumping structure for table ffa.user
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientcode` int(11) DEFAULT NULL,
  `client` int(11) DEFAULT NULL,
  `username` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D64992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_8D93D649A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_8D93D649C05FB297` (`confirmation_token`),
  UNIQUE KEY `UNIQ_8D93D6491DDABAC6` (`clientcode`),
  UNIQUE KEY `UNIQ_8D93D649C7440455` (`client`),
  CONSTRAINT `FK_8D93D6491DDABAC6` FOREIGN KEY (`clientcode`) REFERENCES `client_code` (`id`),
  CONSTRAINT `FK_8D93D649C7440455` FOREIGN KEY (`client`) REFERENCES `clients` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.user: ~3 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `clientcode`, `client`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `image`) VALUES
	(1, 2, 1, 'FHL4601935', 'fhl4601935', 'radjimubarak@gmial.com', 'radjimubarak@gmial.com', 1, 'RJ3/8G4IY4QH6EW6MEUTyb0y1D8f1YIeT3KDBJveinY', '$2y$13$AM64vX4v3gTAw4IPp0H4M.Dj2.5UBOY7Pc5mRcD0a5xVqUa2vJHyO', '2019-12-20 06:16:32', NULL, NULL, 'a:1:{i:0;s:11:"ROLE_CLIENT";}', NULL),
	(2, 1, NULL, 'control', 'control', 'test@example.com', 'test@example.com', 1, 'DOkxdQ5XYZfci7n.9Jsfwt3lp2AjfxOT1Eudkk1yBzg', '$2y$13$ZAZ41eOe7zprR5BGlJhGgOC1iaLK1ENSbez5xf7mTwvrr9jn5CM6m', NULL, NULL, NULL, 'a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}', NULL),
	(3, 3, NULL, 'Manger', 'manger', 'rmobstar01@yahoo.com', 'rmobstar01@yahoo.com', 1, 'hehEB1JfoPXu.HYhWG5M0AHKS3DZ5i.ajMtX0ADPdj0', '$2y$13$/pXI9nyuCq4DKPAID0CEbuBbz08JmJmJGMcKV2CoOFknYwFm08ata', NULL, NULL, NULL, 'a:1:{i:0;s:10:"ROLE_ADMIN";}', NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping structure for table ffa.weekly
DROP TABLE IF EXISTS `weekly`;
CREATE TABLE IF NOT EXISTS `weekly` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `globalPv` int(11) DEFAULT NULL,
  `globalbalance` int(11) DEFAULT NULL,
  `globallevelMerit` int(11) DEFAULT NULL,
  `globallevelLogistic` int(11) DEFAULT NULL,
  `globalgroupBonus` int(11) DEFAULT NULL,
  `globalregbalance` int(11) DEFAULT NULL,
  `globalsalemerite` int(11) DEFAULT NULL,
  `globalsalelogistics` int(11) DEFAULT NULL,
  `globalstartupbonus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table ffa.weekly: ~0 rows (approximately)
/*!40000 ALTER TABLE `weekly` DISABLE KEYS */;
/*!40000 ALTER TABLE `weekly` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
