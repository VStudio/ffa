<?php

namespace App\Form;

use App\Entity\Clients;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstname', TextType::class,[
                'required' => true,
                'label' => 'First name',])
            ->add('lastname', TextType::class,[
                'required' => true,
                'label' => 'Last name',])
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 1,
                    'Female' => 0
                ),
                'required' => true,
                'label' => 'Gender',
            ))
            ->add('email', TextType::class,[
                'required' => true,
                'label' => 'Email',])
            ->add('telephone', IntegerType::class,[
                'required' => true,
                'label' => 'Telephone',])
            ->add('address', TextareaType::class,[
                'required' => false,
                'label' => 'Address',])


            ->add('piecenumber', TextType::class,[
                'required' => false,
                'label' => 'Piece number',
                ])
            ->add('dateofbirth', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Date of birth',
                'html5' => false,
                'required' => false,
                'placeholder' => 'Select a date',
            ])
            ->add('country',EntityType::class, array(
        'class' => 'App\Entity\Pays',
        'choice_label' => 'fr',
        'required' => true,

        'placeholder' => 'Country of origin?'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Clients::class,
        ]);
    }
//    /**
//     * {@inheritdoc}
//     */
//    public function getBlockPrefix()
//    {
//        return 'high_userbundle_clients';
//    }
}
