<?php

namespace App\Form;

use App\Entity\Notifications;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotificationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('object', TextType::class,[
                'required' => true,])
            ->add('description', TextareaType::class,[
                'required' => false,])
            ->add('startdate', DateType::class, [
                'widget' => 'single_text',
                'label' => 'Start date',
                'html5' => false,
                'placeholder' => 'Select a date',
            ])
            ->add('enddate', DateType::class, [
                'widget' => 'single_text',
                'label' => 'End date',
                'html5' => false,
                'placeholder' => 'Select a date',
            ])
//            ->add('enddate')
//            ->add('created')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Notifications::class,
        ]);
    }
}
