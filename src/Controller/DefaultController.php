<?php

namespace App\Controller;

use App\Entity\AdminCommission;
use App\Entity\ClientCode;
use App\Entity\ClientCommission;
use App\Entity\Clients;
use App\Entity\TransPassword;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="verification")
     */
    public function verification()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/acountDetails", name="acountDetails")
     */
    public function acountDetails()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $clientBonus = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $this->getUser()
            ]);
        } elseif (in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            $clientBonus = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $this->getUser()
            ]);
        } elseif (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $clientBonus = $em->getRepository(ClientCommission::class)->findOneBy([
                'client' => $this->getUser()->getClient()
            ]);
        }
        return $this->render('default/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }

    /**
     * @Route("/clients_commissions", name="clientByCom")
     */
    public
    function clientByComAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
//        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
//            $message = "You dont have access to that page";
//            $session->getFlashBag()->add('error', $message);
//            return $this->redirectToRoute('dashboard');
//        }
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository(Clients::class)->getUsersCommission();
        return $this->render('default/clientCommissionList.html.twig', array(
            'liste' => $liste
        ));

    }

    /**
     * @Route("/admin/list", name="admin_list")
     */
    public
    function adminListeAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
//        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
//            return $this->redirectToRoute('fos_user_security_login');
//        }
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository(User::class)->getlisteAdmin();
//        dump($liste);die();
        return $this->render('default/adminListe.html.twig', array(
            'liste' => $liste
        ));
    }


    /**
     * Creates a new administrator.
     *
     * @Route("/new/admin", name="admin_new")
     */
    public function newAdminAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
//        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_CLIENT', $currentuser->getRoles())) {
//            $message = "You dont have access to that page";
//            $session->getFlashBag()->add('error', $message);
//            return $this->redirectToRoute('dashboard');
//        }
        if ($request->getMethod() == 'POST') {
            $username = $request->get('username');
            $email = $request->get('email');
            $telephone = $request->get('telephone');
            $role = $request->get('role');
            $pass = $request->get('pass');
            $confpass = $request->get('confpass');
            $em = $this->getDoctrine()->getManager();
            if ($pass != $confpass) {
                $message = "<b>The passwords are not the same!</b>";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('admin_list');
            }
            $userManager = $this->get('fos_user.user_manager');
//            Create admin code
            $code = 'GP' . substr(mt_rand(), 0, 7);
            $newCode = new ClientCode();
            $newCode->setCreated(new \DateTime());
            $newCode->setStatus(0);
            $newCode->setCode($code);
            $em->persist($newCode);

            // check if email doesn't exist
            $check = $userManager->findUserByEmail($email);
            if (empty($check)) {
                // save to fosusertable
                $user = $userManager->createUser();
                $user->setUsername($username);
                $user->setUsernameCanonical($username);
                $user->setEmail($email);
                $user->setEmailCanonical($email);
                $user->setClientCode($newCode);
                $user->setEnabled(true);
                $user->addRole($role);
//              this method will encrypt the password with the default settings :)
                $user->setPlainPassword($pass);
                $userManager->updateUser($user);
                $message = "<b>Well registered the administrator is created</b> ";
                $session->getFlashBag()->add('success', $message);
//               Create admin wallet
                $newWallet = new AdminCommission();
                $newWallet->setAdmin($user)
                    ->setRegbalance(0.00)
                    ->setBalance(0.00)
                    ->setGroupBonus(0.00);
                $em->persist($newWallet);
                $em->flush();
            } else {
                $message = "<b>This person already exists!</b>";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('admin_list');
            }
            return $this->redirectToRoute('admin_list');

        }

        return $this->render('default/newadmin.html.twig');
    }
    /**
     * Creates a new administrator.
     *
     * @Route("/edit/admin/{id}", name="edit_admin")
     */
    public function editAdminAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
//        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_CLIENT', $currentuser->getRoles())) {
//            $message = "You dont have access to that page";
//            $session->getFlashBag()->add('error', $message);
//            return $this->redirectToRoute('dashboard');
//        }
        $userManager = $this->get('fos_user.user_manager');
        $admin = $userManager->findUserBy(array('id' => $request->get('id')));
        // dump($admin);die();
        if ($request->getMethod() == 'POST') {
            $username = $request->get('username');
            $email = $request->get('email');
            $role = [$request->get('role')];
            // dump($role);die();
            $admin->setUsername($username);
            $admin->setUsernameCanonical($username);
            $admin->setEmail($email);
            $admin->setEmailCanonical($email);
            $admin->setRoles($role);
            $userManager->updateUser($admin);
            $message = "<b>The administrator has modified</b> ";
            $session->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('admin_list');
        }
        // dump($admin);die();
        return $this->render('default/editadmin.html.twig',[
            'admin'=> $admin
        ]);
    }
    /**
     * @Route("/admins/commissions", name="adminByCom")
     */
    public
    function adminByComAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
//        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_CLIENT', $currentuser->getRoles())) {
//            $message = "You dont have access to that page";
//            $session->getFlashBag()->add('error', $message);
//            return $this->redirectToRoute('dashboard');
//        }
        $em = $this->getDoctrine()->getManager();
        $liste = $em->getRepository(Clients::class)->getAdminCommission();
        return $this->render('default/adminCommission.html.twig', array(
            'liste' => $liste
        ));

    }
}
