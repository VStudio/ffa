<?php

namespace App\Controller;

use App\Entity\AdminCommission;
use App\Entity\Branches;
use App\Entity\ClientCommission;
use App\Repository\CommissionsRepository;
use App\Service\LevelSchema;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BonusNetworkController extends AbstractController
{
    /**
     * @Route("/bonus/network", name="bonus_network")
     */
    public function index()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $branch = $em->getRepository(Branches::class)->findOneBy([
            'userid' => $user->getClientCode()->getId()
        ]);

//        dump($branch);
//        die();
        if ($branch != null) {
            $branchlinfo = $em->getRepository(Branches::class)->getFollowerInfo($branch->getUserid()->getId());
            $parent = $branchlinfo[0];
        }else{
            $parent= null;
        }
        return $this->render('bonus_network/tree.html.twig', [
            'userinfo' => $parent
        ]);
    }
    /**
     * @Route("/bonus/commissions", name="bonus_commissions")
     */
    public function commissions()
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        if($user->getClient() != null){
            $com = $em->getRepository(ClientCommission::class)->findOneBy([
                'client' => $user->getClient()
            ]);
        }else{
            $com = $em->getRepository(AdminCommission::class)->findOneBy([
                'client' => $user
            ]);
        }

        return $this->render('bonus_network/clientcommision.html.twig', [
            'bonus' => $com
        ]);
    }
//    /**
//     * @Route("/downline/tree", name=downlinetree")
//     */
//    public
//    function downlinetree(Request $request): Response
//    {
//        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
//        if (!$auth) {
//            return $this->redirectToRoute('fos_user_security_login');
//        }
//        $em = $this->getDoctrine()->getManager();
//        $user = $this->getUser();
//        $branch = $em->getRepository(Branches::class)->findOneBy([
//            'userid' => $user->getClientCode()->getId()
//        ]);
//
////        dump($branch);
////        die();
//        if ($branch != null) {
//            $branchlinfo = $em->getRepository(Branches::class)->getFollowerInfo($branch->getUserid()->getId());
//            $parent = $branchlinfo[0];
//        }else{
//            $parent= null;
//        }
//        return $this->render('bonus_network/tree.html.twig', [
//            'userinfo' => $parent
//        ]);
//    }


    /**
     * @Route("/genelogy", name="genelogy")
     */
    public
    function genelogy(Request $request, LevelSchema $levelSchema): Response
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $branch = $em->getRepository(Branches::class)->findOneBy(array(
            'userid' => $user->getClientcode()
        ));
        //            Level check
        $levelSchema->levelCheck($branch);
//            Apply Commission
        $levelSchema->calcCommission($user);
        $leftbranchStage1 = $rightbranchStage1 = $leftbranchLStage2 = $leftbranchRStage2 = $rightbranchLStage2 = $rightbranchRStage2 = $leftbranchLLStage3 = $leftbranchLRStage3 = $leftbranchRLStage3 = $leftbranchRRStage3 = $rightbranchLLStage3 = $rightbranchLRStage3= $rightbranchRLStage3= $rightbranchRRStage3= null;
        $branchStage1 = $em->getRepository(Branches::class)->getBranchData($user->getClientcode()->getId());

        if ($branchStage1) {
            $leftbranchStage1 = $em->getRepository(Branches::class)->getBranchData($branchStage1[0]['lBranch']
            );
            $rightbranchStage1 = $em->getRepository(Branches::class)->getBranchData($branchStage1[0]['rBranch']
            );

            if ($leftbranchStage1) {
                $leftbranchLStage2 = $em->getRepository(Branches::class)->getBranchData($leftbranchStage1[0]['lBranch']
                );
                $leftbranchRStage2 = $em->getRepository(Branches::class)->getBranchData($leftbranchStage1[0]['rBranch']
                );
            }
            if ($rightbranchStage1) {
                $rightbranchLStage2 = $em->getRepository(Branches::class)->getBranchData($rightbranchStage1[0]['lBranch']
                );
                $rightbranchRStage2 = $em->getRepository(Branches::class)->getBranchData($rightbranchStage1[0]['rBranch']
                );
            }
            if($leftbranchLStage2){
                $leftbranchLLStage3 = $em->getRepository(Branches::class)->getBranchData($leftbranchLStage2[0]['lBranch']
                );
                $leftbranchLRStage3 = $em->getRepository(Branches::class)->getBranchData($leftbranchLStage2[0]['rBranch']
                );
            }
            if($leftbranchRStage2){
                $leftbranchRLStage3 = $em->getRepository(Branches::class)->getBranchData($leftbranchRStage2[0]['lBranch']
                );
                $leftbranchRRStage3 = $em->getRepository(Branches::class)->getBranchData($leftbranchRStage2[0]['rBranch']
                );
            }
            if($rightbranchLStage2){
                $rightbranchLLStage3 = $em->getRepository(Branches::class)->getBranchData($rightbranchLStage2[0]['lBranch']
                );
                $rightbranchLRStage3 = $em->getRepository(Branches::class)->getBranchData($rightbranchLStage2[0]['rBranch']
                );
            }
            if($rightbranchRStage2){
                $rightbranchRLStage3 = $em->getRepository(Branches::class)->getBranchData($rightbranchRStage2[0]['lBranch']
                );
                $rightbranchRRStage3 = $em->getRepository(Branches::class)->getBranchData($rightbranchRStage2[0]['rBranch']
                );
            }
        }

        $branch = $em->getRepository(Branches::class)->findOneBy(array(
            'userid' => $user->getClientcode()
        ));
        $clientCommmission = $em->getRepository(ClientCommission::class)->findOneBy([
            'client' => $user->getClient()
        ]);
        if ($clientCommmission) {
            $balance = $clientCommmission->getBalance();
        } else {
            $balance = 0;
        }
        $allBranch = $em->getRepository(Branches::class)->getPosBranchData($user->getClientcode()->getId());
        if (empty($allBranch)) {
            $expl = 1;
            $val = $user->getClientcode()->getCode();
            $ref = $val . '-' . $expl;
        } else {
            $val = explode('-', $allBranch[0]['ref']);
            $expl = $val[1];
            $ref = $val[0] . '-' . ($expl + 1);
        }

        $levelSchema->levelCheck($branch);
        return $this->render('bonus_network/genelogy.html.twig', [
            'stage1' => $branchStage1,
            'leftbranchStage1' => $leftbranchStage1,
            'rightbranchStage1' => $rightbranchStage1,

            'leftbranchLStage2' => $leftbranchLStage2,
            'leftbranchRStage2' => $leftbranchRStage2,
            'rightbranchLStage2' => $rightbranchLStage2,
            'rightbranchRStage2' => $rightbranchRStage2,
            'leftbranchLLStage3' => $leftbranchLLStage3,
            'leftbranchLRStage3' => $leftbranchLRStage3,
            'leftbranchRLStage3' => $leftbranchRLStage3,
            'leftbranchRRStage3' => $leftbranchRRStage3,
            'rightbranchLLStage3' => $rightbranchLLStage3,
            'rightbranchLRStage3' => $rightbranchLRStage3,
            'rightbranchRLStage3' => $rightbranchRLStage3,
            'rightbranchRRStage3' => $rightbranchRRStage3,
        ]);
    }
}
