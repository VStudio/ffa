<?php

namespace App\Controller;

use App\Entity\AdminCommission;
use App\Entity\Branches;
use App\Entity\ChildLeft;
use App\Entity\ChildRight;
use App\Entity\ClientCode;
use App\Entity\ClientCommission;
use App\Entity\Clients;
use App\Entity\LeftRight;
use App\Entity\Orders;
use App\Entity\RefTable;
use App\Entity\Transaction;
use App\Entity\TransPassword;
use App\Entity\User;
use App\Form\ClientsType;
use App\Repository\ClientsRepository;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/clients")
 */
class ClientsController extends AbstractController
{
    /**
     * @Route("/", name="clients_index", methods={"GET"})
     */
    public function index(): Response
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $session = new Session();
//        if (in_array('ROLE_ADMIN', $currentuser->getRoles()) || in_array('ROLE_CLIENT', $currentuser->getRoles())) {
//            $message = "You dont have access to that page";
//            $session->getFlashBag()->add('success', $message);
//            return $this->redirectToRoute('dashboard');
//        }
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository(User::class)->getlisteClient();
        return $this->render('clients/index.html.twig', [
            'clients' => $clients,
        ]);
    }

    /**
     * @Route("/new", name="clients_new", methods={"GET","POST"})
     */
    public function new(Request $request, UserManagerInterface $userManager): Response
    {
        $client = new Clients();
        $form = $this->createForm(ClientsType::class, $client);
        $form->handleRequest($request);
        $currentuser = $this->getUser();
        $session = new Session();
        $em = $this->getDoctrine()->getManager();

        if (in_array('ROLE_SUPER_ADMIN', $currentuser->getRoles())) {
            $clientBonus = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $this->getUser()
            ]);
        } elseif (in_array('ROLE_ADMIN', $currentuser->getRoles())) {
            $clientBonus = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $this->getUser()
            ]);
        } elseif (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $clientBonus = $em->getRepository(ClientCommission::class)->findOneBy([
                'client' => $this->getUser()->getClient()
            ]);
        }
        if ($form->isSubmitted()) {
// check if email doesn't exist
            $check = $userManager->findUserByEmail($client->getEmail());
            if (!empty($check)) {
                $message = "That email is already taken!";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('clients_new');
            }
            $transactionID = substr(str_shuffle(MD5(microtime())), 0, 11);
            $transferID = substr(str_shuffle(MD5(microtime())), 0, 8);
            $tranpassword = $request->get('tranpassword');
            $username = $client->getFirstname() . substr($client->getLastname(), 0, 1);
            $date = new \DateTime('now');
            $datbasePassword = $em->getRepository(TransPassword::class)->findOneBy([
                'user' => $this->getUser()
            ]);
            if ($datbasePassword === null) {

                $message = "Kindly registrer for a transaction password before performing a transaction <b>E-wallet > Registration</b>";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('dashboard');
            }
            $transpasswordVerif = password_verify($tranpassword, $datbasePassword->getPassword());
//            if ($transpasswordVerif == false) {
//                $message = "The transaction password you entered is incorrect";
//                $session->getFlashBag()->add('error', $message);
//                return $this->redirectToRoute('dashboard');
//            }
//            Check if user has enough fund for the operation
            if ($clientBonus->getRegbalance() < 10) {
                $message = "Your fund do not cover this operation";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('dashboard');
            }
//            Transfer 10$ to superadmin
            $superadmin = $userManager->findUserBy(array('id' => 2));
            if ($superadmin == null) {
                $superadmin = $userManager->findUserBy(array('email' => 'radji@radji.com'));
            }

            $superadminBonus = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $superadmin
            ]);
//            Deduct from client or admin by 10$
            $cpb = $clientBonus->getRegbalance();
            $clientBonus->setRegbalance($cpb - 10);
//            Credit admin by 10$
            $apb = $superadminBonus->getRegbalance();
            $superadminBonus->setRegbalance($apb + 10);
            $em->persist($superadminBonus);
            $em->persist($clientBonus);
            //        Make trasaction
            $transaction = new Transaction();
            $particular = "10$ was deducted
            from your registration commission on for the creation of a new member ( " . $username . " ) " . $date->format('d/m/Y') .
                " at exactly " . $date->format('H:i:s');

            $transaction->setAmount(10);
            $transaction->setTransactionid($transactionID);
            $transaction->setTransferid($transferID);
            $transaction->setType(2);
            $transaction->setSender($currentuser);
            $transaction->setParticulars($particular);
            $transaction->setReceiver($superadmin);
            $em->persist($transaction);
//                    Create client commission
//            $stage= $em->getRepository(Stages::class)->find(1);
            $commission = new ClientCommission();
            $commission->setLevelCommission(0.00);
            $commission->setClient($client);
            $commission->setBalance(0.00);
            $commission->setRegbalance(0.00);
//            $commission->setStage($stage);
            $em->persist($commission);
//                    Create client code
            $rand = substr(str_shuffle('0123456789'), 0, 7);
            $clientCode = new ClientCode();
            $clientCode->setActivatedDate(new \DateTime());
            $clientCode->setCreated(new \DateTime());
            $clientCode->setStatus(1);
            $clientCode->setCode('FHL' . $rand);
            $em->persist($clientCode);

//                    Do the branching
            $refTable = $em->getRepository(RefTable::class)->find(1);
            if (empty($refTable) || $refTable == null) {
                $refTable = new RefTable();
                $refTable->setRef('FLREF-1-');
                $refTable->setNiveau(1);
                $em->persist($refTable);
                $em->flush();
                $ref = 'FLREF-1-';
                $refUni = mt_rand(10, time());
                $parentLeft = $em->getRepository(LeftRight::class)->find(1);

                $newChildLeft = new ChildLeft();
                $newChildLeft->setRef($refUni)
                    ->setLeftleft(1)
                    ->setLeftright(0)
                    ->setParent($parentLeft);
                $em->persist($newChildLeft);
                $parentRight = $em->getRepository(LeftRight::class)->find(2);
                $newChildRight = new ChildRight();
                $newChildRight->setRef($refUni)
                    ->setRightleft(1)
                    ->setRightright(0)
                    ->setParent($parentRight);
                $em->persist($newChildRight);

                $newBranch = new Branches();
                $newBranch->setNames($client->getLastname() . ' ' . $client->getFirstname())
                    ->setLevel(0)
                    ->setUserid($clientCode)
                    ->setFollwers(0)
                    ->setRef($refUni);
                $em->persist($newBranch);
            } else {
                $ref = $refTable->getRef();

                $refValue = explode('-', $ref);

                $branch = $em->getRepository(Branches::class)->getLastBranch();
                $childLeft = $em->getRepository(ChildLeft::class)->findOneBy(['ref' => $branch[0]->getRef()]);
                if ($childLeft->getLeftleft() > 0) {
                    $branch[0]->setLBranch($clientCode)
                        ->setLevel(0)
                        ->setFollowers(1);
                    $em->persist($branch);

                    $refTable->setRef('FLREF-' . $refValue[1] . '-R');
                    $em->persist($refTable[0]);

                    $childLeft->setLeftleft($childLeft->getLeftleft() - 1);
                } elseif ($childLeft->getLeftright() > 0) {
                    $branch[0]->setRBranch($clientCode)
                        ->setLevel(1)
                        ->setFollowers(2);
                    $em->persist($branch);
                    $newRefId = (int)$refValue[1] + 1;
                    $refTable[0]->setRef('FLREF-' . $newRefId . '-L');
                    $em->persist($refTable[0]);
                    $childLeft->setLeftright($childLeft->getLeftright() - 1);
                } elseif ($childLeft->getLeftleft() == 0 && $childLeft->getLeftright() == 0) {

                    $childRight = $em->getRepository(ChildRight::class)->findOneBy(['ref' => $branch[0]->getRef()]);
                    if ($childRight->getRightleft() > 0) {

                    } elseif ($childRight->getRightright() > 0) {

                    } elseif ($childRight->getRightleft() == 0 && $childRight->getRightright() == 0) {
//Take last line created in branch and make parent left
                    }
                }


                $ref = $refTable->getRef();
            }
            $refValue = explode('-', $ref);
            if ($refValue[2] == '' || empty($refValue[2])) {


                $refTable[0]->setRef('FLREF-1-L');
                $em->persist($refTable[0]);
            } else {
                $branch = $em->getRepository(Branches::class)->findOneBy(array(
                    'id' => $refValue[1]
                ));
                if ($branch != null) {

                    if ($refValue[2] == 'L') {
                        $branch->setLBranch($clientCode)
                            ->setLevel(0)
                            ->setFollowers(1);
                        $em->persist($branch);

                        $refTable[0]->setRef('FLREF-' . $refValue[1] . '-R');
                        $em->persist($refTable[0]);
                    } elseif ($refValue[2] == 'R') {
                        $branch->setRBranch($clientCode)
                            ->setLevel(1)
                            ->setFollowers(2);
                        $em->persist($branch);

                        $newRefId = (int)$refValue[1] + 1;
                        $refTable[0]->setRef('FLREF-' . $newRefId . '-L');
                        $em->persist($refTable[0]);
                    }
                } else {
                    $newBranch = new Branches();
                    $newBranch->setNames($client->getLastname() . ' ' . $client->getFirstname())
                        ->setLevel(0)
                        ->setUserid($clientCode)
                        ->setFollwers(0)
                        ->setRef($ref);
                    $em->persist($newBranch);
                    $refTable[0]->setRef('FLREF-' . $refValue[1] . '-L');
                    $em->persist($refTable[0]);
                }


            }

//            if($branch == null || empty($branch)){
//                $newBranch = new Branches();
//
//            }

            $client->setAgreed(1);
            $em->persist($client);
            $em->flush();
            //                Create the user
            /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
            $user = $userManager->createUser();
            $user->setUsername($clientCode->getCode());
            $user->setUsernameCanonical($clientCode->getCode());
            $user->setEmail($client->getEmail());
            $user->setEmailCanonical($client->getEmail());
            $user->setEnabled(true);
            $user->addRole('ROLE_CLIENT');
            $user->setClient($client);
            $user->setClientcode($clientCode);
            // this method will encrypt the password with the default settings :)
            $user->setPlainPassword('azerty123');
            $userManager->updateUser($user);
            $orders = $em->getRepository(Orders::class)->findAll();
            $totalorder = count($orders);
            $order = new Orders();
            $order->setUser($currentuser)
                ->setRegisteredUser($user)
                ->setType('Registration')
                ->setOrdernumber('HL100' . ($totalorder + 1));
            $em->persist($order);

            $em->flush();
            $message = "You have sucessfully registered the member: <b>" . $clientCode->getCode() . "</b> with the password: <b> azerty123 </b>";
            $session->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('clients_new');
        }

        return $this->render('clients/new.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="clients_show", methods={"GET"})
     */
    public function show(Clients $client): Response
    {
        return $this->render('clients/show.html.twig', [
            'client' => $client,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="clients_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Clients $client): Response
    {
        $form = $this->createForm(ClientsType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clients_index');
        }

        return $this->render('clients/edit.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="clients_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Clients $client): Response
    {
        if ($this->isCsrfTokenValid('delete' . $client->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($client);
            $entityManager->flush();
        }

        return $this->redirectToRoute('clients_index');
    }

    /**
     * @Route("/reset_passe/{id}",name="reset_passe")
     */
    public function reset_passeAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $id = $request->get('id');
        $session = new Session();
        $currentuser = $this->getUser();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $session->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
//            Set new password
        $newpasse = '123@password';
        $newpassword = $encoder->encodePassword($newpasse, $user->getSalt());
        $user->setPassword($newpassword);
        $userManager->updateUser($user);
        $message = "You have just reinitialised the password of " . $user->getUsername();
        $session->getFlashBag()->add('success', $message);
        if (in_array('ROLE_ADMIN', $user->getRoles()) || in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {
            return $this->redirectToRoute('dashboard');
        }
        return $this->redirectToRoute('dashboard');
    }

    /**
     * @Route("/changeStat/{stat}/{id}",name="changeStat")
     */
    public function changeStatAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }

        $currentuser = $this->getUser();
        $session = new Session();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $session->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $stat = $request->get('stat');
        $id = $request->get('id');
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserBy(array('id' => $id));
        // dump($user->getUsername());die();

        $user->setEnabled($stat);
        $userManager->updateUser($user);
        if ($stat == 1) {
            $message = "The account of <b>" . $user->getUsername() . "</b> has been activated";
        } else {
            $message = "The account of <b>" . $user->getUsername() . "</b> has been deactivated";
        }


        $session->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('clients_index');
    }
}
