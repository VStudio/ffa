<?php

namespace App\Controller;

use App\Entity\AdminCommission;
use App\Entity\ClientCode;
use App\Entity\ClientCommission;
use App\Entity\Transaction;
use App\Entity\TransPassword;
use App\Service\CommissionImpact;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class TransactionController extends AbstractController
{

    /**
     * @Route("/transaction", name="transaction")
     */
    public function indexAction(Request $request, CommissionImpact $commissionImpact,UserManagerInterface $userManager)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $em = $this->getDoctrine()->getManager();
        $balance = $em->getRepository(ClientCommission::class)->findOneBy([
            'client' => $this->getUser()->getClient()
        ]);
        if ($balance == null) {
            $balance = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $this->getUser()
            ]);
        }
        if ($request->getMethod() == 'POST') {
            $session = new Session();
            $transactionID = substr(str_shuffle(MD5(microtime())), 0, 11);
            $transferID = substr(str_shuffle(MD5(microtime())), 0, 8);
            $recipientcode = $request->get('recipient');
            $amount = $request->get('amount');
            $type = $request->get('type');
            $fund = $request->get('fund');
            $password = $request->get('password');
            $sender = $this->getUser();
            $date = new \DateTime('now');

//        Check if password is correct
            $datbasePassword = $em->getRepository(TransPassword::class)->findOneBy([
                'user' => $sender
            ]);
            if ($datbasePassword === null) {

                $message = "Kindly registrer for a transaction password before performing a transaction <b>E-wallet > Registration</b>";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('transaction');
            }
            if (password_verify($password, $datbasePassword->getPassword())) {

//        Check the user has enough comission
                $confirmcommission = $em->getRepository(ClientCommission::class)->findOneBy([
                    'client' => $sender->getClient()
                ]);
//            dump($this->checkBalance($amount, $fund, $confirmcommission));die();
                if ($confirmcommission == null) {
                    $confirmcommission = $em->getRepository(AdminCommission::class)->findOneBy([
                        'admin' => $sender
                    ]);
                }
                if ($this->checkBalance($amount, $fund, $confirmcommission)) {
                    $message = "The amount you entered is greater than your commission balance";
                    $session->getFlashBag()->add('error', $message);
                    return $this->redirectToRoute('transaction');
                }


//        Make trasaction
                $transaction = new Transaction();

                //        Check if $recipient exists
                $recipient = $em->getRepository(ClientCode::class)->findOneBy([
                    'code' => $recipientcode
                ]);
                if ($recipient === null) {

                    $message = "The recipient code you entered doesn't exist";
                    $session->getFlashBag()->add('error', $message);
                    return $this->redirectToRoute('transaction');
                }
                $receiver = $userManager->findUserBy(array('clientcode' => $recipient));
                $particular = $amount . "$ was paid to " . $receiver->getUsername() . "'s " . strtoupper($fund) . " account on " . $date->format('d/m/Y');
                $transaction->setAmount($amount);
                $transaction->setTransactionid($transactionID);
                $transaction->setTransferid($transferID);
                $transaction->setType(1);
                $transaction->setSender($sender);
                $transaction->setParticulars($particular);
                $transaction->setReceiver($receiver);
//            impact commission
//            dump();die();
                if ($commissionImpact->impactCommission($sender, $receiver, $amount, $fund) == false) {
                    $message = "Your transaction was not done";
                    $session->getFlashBag()->add('success', $message);
                    return $this->redirectToRoute('dashboard');
                }

                $message = "Your transaction was successful";
                $session->getFlashBag()->add('success', $message);
                $em->persist($transaction);
                $em->flush();
                return $this->redirectToRoute('dashboard');
            } else {
                $message = "The transaction password you entered is incorrect";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('transaction');
            }
        }
        return $this->render('transaction/transfer.html.twig', [
            'client' => $this->getUser()->getClient(),
            'commission' => $balance
        ]);
    }
    /**
     * @Route("/transaction_history", name="transaction_history")
     */
    public
    function transferHistoryAction(Request $request)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $em = $this->getDoctrine()->getManager();
        $Ttransaction = $em->getRepository(Transaction::class)->findBy(
            ['sender' => $this->getUser()], ['id' => 'DESC']);

        $Rtransaction = $em->getRepository(Transaction::class)->findBy(
            ['receiver' => $this->getUser()], ['id' => 'DESC']);
        return $this->render('transaction/transferHistory.htm.twig', [
            'Ttransaction' => $Ttransaction,
            'Rtransaction' => $Rtransaction
        ]);
    }
    private
    function checkBalance($amount, $fund, $confirmcommission)
    {
        if ($fund == 'levelCommission') {
            if ($amount >= $confirmcommission->getBalance() || $confirmcommission->getBalance() <= 10) {
                return true;
            }
        } elseif ($fund == 'registration') {
            if ($amount >= $confirmcommission->getRegbalance() || $confirmcommission->getRegbalance() <= 10) {
                return true;
            }
        } elseif ($fund == 'bottles') {
            if ($amount >= $confirmcommission->getBottles() || $confirmcommission->getBottles() <=10) {
                return true;
            }
        } elseif ($fund == 'matchingBonus') {
            if ($amount >= $confirmcommission->getMatchingBonus() || $confirmcommission->getMatchingBonus() <=10) {
                return true;
            }
        } elseif ($fund == 'teamSaleBonus') {
            if ($amount >= $confirmcommission->getTeamSaleBonus() ||$confirmcommission->getTeamSaleBonus() <= 10) {
                return true;
            }
        } elseif ($fund == 'startupbonus') {
            if ($amount >= $confirmcommission->getStartupbonus() || $confirmcommission->getStartupbonus()<= 10) {
                return true;
            }
        } elseif ($fund == 'productVoucherBonus') {
            if ($amount >= $confirmcommission->getProductVoucherBonus() || $confirmcommission->getProductVoucherBonus() <= 10) {
                return true;
            }
        } elseif ($fund == 'eCoin') {
            if ($amount >= $confirmcommission->getECoin() || $confirmcommission->getECoin() <= 10) {
                return true;
            }
        } elseif ($fund == 'euCoin') {
            if ($amount >= $confirmcommission->getEuCoin() || $confirmcommission->getEuCoin() <= 10) {
                return true;
            }
        } elseif ($fund == 'saleAward') {
            if ($amount >= $confirmcommission->getSaleAward() || $confirmcommission->getSaleAward() <= 10) {
                return true;
            }
        }
        elseif ($fund == 'payBack') {
            if ($amount >= $confirmcommission->getPayBack() || $confirmcommission->getPayBack() <= 10) {
                return true;
            }
        }

        return false;
    }
}
