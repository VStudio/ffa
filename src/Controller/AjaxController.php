<?php

namespace App\Controller;

use App\Entity\Branches;
use App\Entity\ClientCode;
use App\Entity\ClientCommission;
use FOS\UserBundle\Model\UserManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AjaxController extends AbstractController
{
    /**
     * Search for sponsor info
     * @Route("/getBranches", name="getBranches", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function getBranchesAction(Request $request,UserManagerInterface $userManager)
    {
        if ($request->isXmlHttpRequest()) {
            $user = $request->get('user');
            $em = $this->getDoctrine()->getManager();
            $left = [];
            $right = [];
            $parent = [];
            $userinfo = [];
            $clientcode = $em->getRepository(ClientCode::class)->findOneBy([
                'code' => $user
            ]);

            $branch = $em->getRepository(Branches::class)->findOneBy(array(
                'userid' => $clientcode
            ));

            if ($branch != null) {
                $parent = $em->getRepository(Branches::class)->getFollowerInfo($branch->getUserid()->getId());
//                dump($parent);die();
                if ($branch->getLBranch() != null) {
                    $left = $em->getRepository(Branches::class)->getFollowerInfo($branch->getLBranch()->getId());
                }
                if ($branch->getRBranch() != null) {
                    $right = $em->getRepository(Branches::class)->getFollowerInfo($branch->getRBranch()->getId());
                }
            } else {
                $userinfo = $userManager->findUserBy(array('clientcode' => $clientcode));
//            dump($parent,$left,$right);die();

                $userinfo = $em->getRepository(Branches::class)->getFollowerInfo($userinfo->getClientCode()->getId());
//                dump($userinfo);
//                die();

            }
            $response = array(
                "code" => 200,
                "response" => $this->render('ajax/createtree.html.twig', array(
                    'parent' => $parent,
                    'left' => $left,
                    'right' => $right,
                    'user' => $userinfo
                ))->getContent() );
            return new JsonResponse($response);
            return $this->container->get('templating')->renderResponse('ajax/createtree.html.twig', array(
                'parent' => $parent,
                'left' => $left,
                'right' => $right,
                'user' => $userinfo
            ));
        }
    }

    /**
     * Search for sponsor info
     * @Route("/checkSponorCode", name="checkSponorCode", options= {"expose" = true})
     * @Method({"POST"})
     */
    public function checkSponorCodeAction(Request $request,UserManagerInterface $userManager)
    {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $sponsor = $request->get('sponsorcode');
//            Check if code exists
            $sponsorCode = $em->getRepository(ClientCode::class)->findOneBy(array(
                'code' => $sponsor,
                'status' => 1
            ));
//            If sponsor code is not empty look for the user linked to code
            if (!empty($sponsorCode)) {
                /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
                $checkUserByCodes = $userManager->findUserBy(array(
                    'clientcode' => $sponsorCode->getId(),
                ));
                $recipientCom= $em->getRepository(ClientCommission::class)->findOneBy([
                    'client'=>$checkUserByCodes->getClient()
                ]);
                $recipientBranch= $em->getRepository(Branches::class)->findOneBy([
                    'userid'=>$checkUserByCodes->getClientCode()
                ]);
//                dump($checkUserByCodes);die();
                return $this->container->get('templating')->renderResponse('ajax/userinfo.html.twig', array(
                    'recipientCom' => $recipientCom,
                    'recipientInfo' => $checkUserByCodes,
                    'recipientBranch' => $recipientBranch,
                ));
//                return new Response('User found: <b style="color: limegreen">'
//                    . $checkUserByCodes->getClient()->getLastname() .
//                    ' '
//                    . $checkUserByCodes->getClient()->getFirstname() .
//                    ' <i class="fas fa-check"></i></b> ');
            } else {
                return new Response('<b style="color: firebrick">This code either does not valide or dose not exsit with us !!!</b>');
            }
        }
    }
}
