<?php

namespace App\Controller;

use App\Entity\TransPassword;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class TransactionPasswordController extends AbstractController
{
    /**
     * @Route("/transaction/password", name="transactionpassword")
     */
    public function index()
    {
//        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
//        if (!$auth) {
//            return $this->redirectToRoute('fos_user_security_login');
//        }
        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $existpassword = $em->getRepository(TransPassword::class)->findOneBy([
            'user' => $this->getUser()
        ]);
        return $this->render('transaction_password/transpassword.html.twig', [
            'existpassword' => $existpassword
        ]);
    }
    /**
     * @Route("/create/password", name="createpassword")
     */
    public function create(Request $request)
    {
//        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
//        if (!$auth) {
//            return $this->redirectToRoute('fos_user_security_login');
//        }
        $session = new Session();
        $em = $this->getDoctrine()->getManager();

        $password = $request->get('newpassword');
        $confpassword = $request->get('confpassword');
        $existpassword = $em->getRepository(TransPassword::class)->findOneBy([
            'user' => $this->getUser()
        ]);
        if (!empty($existpassword) && $existpassword != null) {
            $message = "You already have a transaction password! If you wish to update go to <u>Settings > Update Transaction Password</u>";
            $session->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('transactionpassword');
        }
        if ($password === $confpassword) {


            $hashed_password = password_hash($password, PASSWORD_BCRYPT);
            $transpassword = new TransPassword();
            $transpassword->setUser($this->getUser());
            $transpassword->setPassword($hashed_password);
            $em->persist($transpassword);
            $em->flush();
            $message = "You  have sucessfully registered for a transaction password!";
            $session->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('transactionpassword');
        }

        $message = "The passwords do not match";
        $session->getFlashBag()->add('error', $message);
        return $this->redirectToRoute('transactionpassword');
    }

    /**
     * @Route("/update/password", name="update_password")
     */
    public function updatePassword(Request $request)
    {
//        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
//        if (!$auth) {
//            return $this->redirectToRoute('fos_user_security_login');
//        }

        $session = new Session();
        $em = $this->getDoctrine()->getManager();
        $password = $request->get('password');
        $newpassword = $request->get('newpassword');
        $confpassword = $request->get('confpassword');
        $transactionpassword = $em->getRepository(TransPassword::class)->findOneBy([
            'user' => $this->getUser()
        ]);


        if (empty($transactionpassword) && $transactionpassword !== null) {
            $message = "You don't have a transaction password! kindly register for one <b>E-wallet > Transaction password</b>";
            $session->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('transactionpassword');
        }
        if (password_verify($password, $transactionpassword->getPassword())) {
            if ($newpassword === $confpassword) {

                $hashedPassword = password_hash($newpassword, PASSWORD_BCRYPT);
                $transactionpassword->setPassword($hashedPassword);
                $em->persist($transactionpassword);
                $em->flush();
                $message = "Your transaction password has been successgully updated";
                $session->getFlashBag()->add('success', $message);
                return $this->redirectToRoute('transactionpassword');
            } else {
                $message = "The passwords you entered do not match";
                $session->getFlashBag()->add('error', $message);
                return $this->redirectToRoute('transactionpassword');
            }
        } else {
            $message = "The transaction password you entered is incorrect, if you have forgotten you password kindly contact out support team";
            $session->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('transactionpassword');
        }

        return $this->redirectToRoute('transactionpassword');

    }

    /**
     * @Route("/reset_transaction_password/{id}", name="reset_transaction_password")
     */
    public function adminreset(Request $request,UserManagerInterface $userManager)
    {
        $auth = $this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY');
        if (!$auth) {
            return $this->redirectToRoute('fos_user_security_login');
        }
        $currentuser = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $session = new Session();
        if (in_array('ROLE_CLIENT', $currentuser->getRoles())) {
            $message = "You dont have access to that page";
            $session->getFlashBag()->add('success', $message);
            return $this->redirectToRoute('dashboard');
        }
        $id = $request->get('id');
        $user = $userManager->findUserBy(array('id' => $id));
        $transactionpassword = $em->getRepository(TransPassword::class)->findOneBy([
            'user' => $user
        ]);
        if ($transactionpassword === null) {
            $message = "Kindly inform the user to register for a transaction password before performing a reset";
            $session->getFlashBag()->add('error', $message);
            return $this->redirectToRoute('dashboard');
        }
        $restepassword = 'azerty123';
        $hashedPassword = password_hash($restepassword, PASSWORD_BCRYPT);
        $transactionpassword->setPassword($hashedPassword);
        $em->persist($transactionpassword);
        $em->flush();
        $message = "Transaction password has been reset successfully ";
        $session->getFlashBag()->add('success', $message);
        return $this->redirectToRoute('dashboard');
    }

}
