<?php
/**
 * Created by PhpStorm.
 * User: utilisateur
 * Date: 1/13/2020
 * Time: 12:11 PM
 */

namespace App\Service;


use App\Entity\AdminCommission;
use App\Entity\ClientCommission;
use Symfony\Component\DependencyInjection\Container;
class CommissionImpact
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }
    public
    function impactCommission($sender, $reciever, $amount, $fund)
    {
        $em = $this->container->get('doctrine')->getManager();
        $sendercommission = $em->getRepository(ClientCommission::class)->findOneBy([
            'client' => $sender->getClient()
        ]);

        if ($sendercommission == null) {
            $sendercommission = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $sender
            ]);
        }

        $recievercommission = $em->getRepository(ClientCommission::class)->findOneBy([
            'client' => $reciever->getClient()
        ]);

        if ($recievercommission == null) {
            $recievercommission = $em->getRepository(AdminCommission::class)->findOneBy([
                'admin' => $reciever
            ]);
        }

        if ($fund == 'levelCommission') {
            //            Sender Acc
            $senderBalance = $sendercommission->getBalance();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setBalance($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getBalance();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setBalance($newbalance);
        } elseif ($fund == 'registration') {
            //            Sender Acc
            $senderBalance = $sendercommission->getRegbalance();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setRegbalance($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getRegbalance();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setRegbalance($newbalance);
        } elseif ($fund == 'bottles') {
            //            Sender Acc
            $senderBalance = $sendercommission->getBottles();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setBottles($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getBottles();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setBottles($newbalance);
        } elseif ($fund == 'teamSaleBonus') {
            //            Sender Acc
            $senderBalance = $sendercommission->getTeamSaleBonus();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setTeamSaleBonus($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getTeamSaleBonus();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setTeamSaleBonus($newbalance);
        } elseif ($fund == 'matchingBonus') {
            //            Sender Acc
            $senderBalance = $sendercommission->getMatchingBonus();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setMatchingBonus($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getMatchingBonus();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setMatchingBonus($newbalance);
        } elseif ($fund == 'startupbonus') {
            //            Sender Acc
            $senderBalance = $sendercommission->getStartupbonus();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setStartupbonus($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getStartupbonus();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setStartupbonus($newbalance);
        }
        elseif ($fund == 'eCoin') {
            //            Sender Acc
            $senderBalance = $sendercommission->getECoin();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setECoin($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getECoin();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setECoin($newbalance);
        }
        elseif ($fund == 'euCoin') {
            //            Sender Acc
            $senderBalance = $sendercommission->getEuCoin();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setEuCoin($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getEuCoin();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setEuCoin($newbalance);
        }
        elseif ($fund == 'productVoucherBonus') {
            //            Sender Acc
            $senderBalance = $sendercommission->getProductVoucherBonus();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setProductVoucherBonus($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getProductVoucherBonus();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setProductVoucherBonus($newbalance);
        }
        elseif ($fund == 'payBack') {
            //            Sender Acc
            $senderBalance = $sendercommission->getPayBack();
            $newbalance = $senderBalance - $amount;
            $sendercommission->setPayBack($newbalance);
            //            Reciever Acc
            $recieverBalance = $recievercommission->getPayBack();
            $newbalance = $recieverBalance + $amount;
            $recievercommission->setPayBack($newbalance);
        }

        $em->persist($sendercommission);


        $em->persist($recievercommission);

        $em->flush();

        return true;
    }
}