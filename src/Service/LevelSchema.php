<?php
/**
 * Created by PhpStorm.
 * User: utilisateur
 * Date: 12/14/2019
 * Time: 9:25 AM
 */

namespace App\Service;

use App\Entity\Branches;
use App\Entity\ClientCommission;
use App\Entity\Transaction;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Response;
class LevelSchema
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public
    function levelCheck($branch)
    {
        $em = $this->container->get('doctrine')->getManager();
        $leftbranchStage1 = null;
        $rightbranchStage1 = null;
        $leftbranchLStage2 = null;
        $leftbranchRStage2 = null;
        $rightbranchLStage2 = null;
        $rightbranchRStage2 = null;
        if ($branch) {
            $leftbranchStage1 = $em->getRepository(Branches::class)->findOneBy(['userid' => $branch->getLBranch()]);
            if ($branch->getRBranch() != null) {
                $rightbranchStage1 = $em->getRepository(Branches::class)->findOneBy(['userid' => $branch->getRBranch()]);
            }

            if ($leftbranchStage1) {
                $leftbranchLStage2 = $em->getRepository(Branches::class)->findOneBy(['userid' => $leftbranchStage1->getLBranch()]);
                $leftbranchRStage2 = $em->getRepository(Branches::class)->findOneBy(['userid' => $leftbranchStage1->getRBranch()]
                );
                if($leftbranchLStage2){
                    $leftbranchLLStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $leftbranchLStage2->getLBranch()]);
                    $leftbranchLRStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $leftbranchLStage2->getRBranch()]);
                }
                if($leftbranchRStage2){
                    $leftbranchRLStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $leftbranchRStage2->getLBranch()]);
                    $leftbranchRRStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $leftbranchRStage2->getRBranch()]);
                }
            }
            if ($rightbranchStage1) {
                $rightbranchLStage2 = $em->getRepository(Branches::class)->findOneBy(['userid' => $rightbranchStage1->getLBranch()]);
                $rightbranchRStage2 = $em->getRepository(Branches::class)->findOneBy(['userid' => $rightbranchStage1->getRBranch()]);
                if($rightbranchLStage2){
                    $rightbranchLLStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $rightbranchLStage2->getLBranch()]);
                    $rightbranchLRStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $rightbranchLStage2->getRBranch()]);
                }
                if($rightbranchRStage2){
                    $rightbranchRLStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $rightbranchRStage2->getLBranch()]);
                    $rightbranchRRStage3= $em->getRepository(Branches::class)->findOneBy(['userid' => $rightbranchRStage2->getRBranch()]);
                }
            }
        }
        //dump($leftbranchStage1,$rightbranchStage1,$leftbranchLStage2,$leftbranchRStage2,$rightbranchLStage2,$rightbranchRStage2,$leftbranchLLStage3,$leftbranchLRStage3,$leftbranchRLStage3,$leftbranchRRStage3,$rightbranchLLStage3,$rightbranchLRStage3,$rightbranchRLStage3,$rightbranchRRStage3);die();
        if ($branch !== NULL) {
//            Check level
            if ($leftbranchStage1 != null && $leftbranchStage1->getFollwers() == 2 && $rightbranchStage1 != null && $rightbranchStage1->getFollwers() == 2) {
                $branch->setLevel(1);
                $em->persist($branch);
            }

            if ($leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null) {
//            Check level
                if ($leftbranchLStage2->getFollwers() >= 2 && $leftbranchRStage2->getFollwers() >= 2 && $rightbranchLStage2->getFollwers() >= 2 && $rightbranchRStage2->getFollwers() >= 2) {
                    $branch->setLevel(2);
                    $em->persist($branch);
                }
            }

            //$levelCounter
            if ($leftbranchStage1 != null && $rightbranchStage1 != null && $leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null && $leftbranchLLStage3!= null && $leftbranchLRStage3!= null && $leftbranchRLStage3!= null && $leftbranchRRStage3!= null && $rightbranchLLStage3!= null && $rightbranchLRStage3!= null && $rightbranchRLStage3!= null && $rightbranchRRStage3!= null) {
//            Check level
                if ($leftbranchStage1->getLevel() >= 2 && $rightbranchStage1->getLevel() >= 2 && $leftbranchLStage2->getLevel() >= 2 && $leftbranchRStage2->getLevel() >= 2 && $rightbranchLStage2->getLevel() >= 2 && $rightbranchRStage2->getLevel() >= 2 && $leftbranchLRStage3->getLevel() >= 2 && $leftbranchRLStage3->getLevel() >= 2 && $leftbranchRRStage3->getLevel() >= 2 && $rightbranchLLStage3->getLevel() >= 2 && $rightbranchLRStage3->getLevel() >= 2 && $rightbranchRLStage3->getLevel() >= 2 && $rightbranchRRStage3->getLevel() >= 2) {
                    $branch->setLevel(3);
                    $em->persist($branch);
                }
            }

            if ($leftbranchStage1 != null && $rightbranchStage1 != null && $leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null && $leftbranchLLStage3!= null && $leftbranchLRStage3!= null && $leftbranchRLStage3!= null && $leftbranchRRStage3!= null && $rightbranchLLStage3!= null && $rightbranchLRStage3!= null && $rightbranchRLStage3!= null && $rightbranchRRStage3!= null) {
//            Check level
                if ($leftbranchStage1->getLevel() >= 3 && $rightbranchStage1->getLevel() >= 3 && $leftbranchLStage2->getLevel() >= 3 && $leftbranchRStage2->getLevel() >= 3 && $rightbranchLStage2->getLevel() >= 3 && $rightbranchRStage2->getLevel() >= 3 && $leftbranchLRStage3->getLevel() >= 3 && $leftbranchRLStage3->getLevel() >= 3 && $leftbranchRRStage3->getLevel() == 3 && $rightbranchLLStage3->getLevel() >= 3 && $rightbranchLRStage3->getLevel() >= 3 && $rightbranchRLStage3->getLevel() >= 3 && $rightbranchRRStage3->getLevel() >= 3) {
                    $branch->setLevel(4);
                    $em->persist($branch);

                }
            }

            if ($leftbranchStage1 != null && $rightbranchStage1 != null && $leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null && $leftbranchLLStage3!= null && $leftbranchLRStage3!= null && $leftbranchRLStage3!= null && $leftbranchRRStage3!= null && $rightbranchLLStage3!= null && $rightbranchLRStage3!= null && $rightbranchRLStage3!= null && $rightbranchRRStage3!= null) {
//            Check level
                if ($leftbranchStage1->getLevel() >= 4 && $rightbranchStage1->getLevel() >= 4 && $leftbranchLStage2->getLevel() >= 4 && $leftbranchRStage2->getLevel() >= 4 && $rightbranchLStage2->getLevel() >= 4 && $rightbranchRStage2->getLevel() >= 4 && $leftbranchLRStage3->getLevel() >= 4 && $leftbranchRLStage3->getLevel() >= 4 && $leftbranchRRStage3->getLevel() >= 4 && $rightbranchLLStage3->getLevel() >= 4 && $rightbranchLRStage3->getLevel() >= 4 && $rightbranchRLStage3->getLevel() >= 4 && $rightbranchRRStage3->getLevel() >= 4) {

                    $branch->setLevel(5);
                    $em->persist($branch);

                }
            }

            if ($leftbranchStage1 != null && $rightbranchStage1 != null && $leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null && $leftbranchLLStage3!= null && $leftbranchLRStage3!= null && $leftbranchRLStage3!= null && $leftbranchRRStage3!= null && $rightbranchLLStage3!= null && $rightbranchLRStage3!= null && $rightbranchRLStage3!= null && $rightbranchRRStage3!= null) {
//            Check level
                if ($leftbranchStage1->getLevel() >= 5 && $rightbranchStage1->getLevel() >= 5 && $leftbranchLStage2->getLevel() >= 5 && $leftbranchRStage2->getLevel() >= 5 && $rightbranchLStage2->getLevel() >= 5 && $rightbranchRStage2->getLevel() >= 5 && $leftbranchLRStage3->getLevel() >= 5 && $leftbranchRLStage3->getLevel() >= 5 && $leftbranchRRStage3->getLevel() >= 5 && $rightbranchLLStage3->getLevel() >= 5 && $rightbranchLRStage3->getLevel() == 5 && $rightbranchRLStage3->getLevel() >= 5 && $rightbranchRRStage3->getLevel() >= 5) {
                    $branch->setLevel(6);
                    $em->persist($branch);

                }
            }

            if ($leftbranchStage1 != null && $rightbranchStage1 != null && $leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null && $leftbranchLLStage3!= null && $leftbranchLRStage3!= null && $leftbranchRLStage3!= null && $leftbranchRRStage3!= null && $rightbranchLLStage3!= null && $rightbranchLRStage3!= null && $rightbranchRLStage3!= null && $rightbranchRRStage3!= null) {
//            Check level
                if ($leftbranchStage1->getLevel() >= 6 && $rightbranchStage1->getLevel() >= 6 && $leftbranchLStage2->getLevel() >= 6 && $leftbranchRStage2->getLevel() >= 6 && $rightbranchLStage2->getLevel() >= 6 && $rightbranchRStage2->getLevel() >= 6 && $leftbranchLRStage3->getLevel() == 6 && $leftbranchRLStage3->getLevel() >= 6 && $leftbranchRRStage3->getLevel() >= 6 && $rightbranchLLStage3->getLevel() == 6 && $rightbranchLRStage3->getLevel() >= 6 && $rightbranchRLStage3->getLevel() >= 6 && $rightbranchRRStage3->getLevel() >= 6) {
                    $branch->setLevel(7);
                    $em->persist($branch);
                }
            }

            if ($leftbranchStage1 != null && $rightbranchStage1 != null && $leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null && $leftbranchLLStage3!= null && $leftbranchLRStage3!= null && $leftbranchRLStage3!= null && $leftbranchRRStage3!= null && $rightbranchLLStage3!= null && $rightbranchLRStage3!= null && $rightbranchRLStage3!= null && $rightbranchRRStage3!= null) {
//            Check level
                if ($leftbranchStage1->getLevel() >=7 && $rightbranchStage1->getLevel() >= 7 && $leftbranchLStage2->getLevel() >= 7 && $leftbranchRStage2->getLevel() >= 7 && $rightbranchLStage2->getLevel() == 7 && $rightbranchRStage2->getLevel() >= 7 && $leftbranchLRStage3->getLevel() >= 7 && $leftbranchRLStage3->getLevel() >= 7 && $leftbranchRRStage3->getLevel() >= 7 && $rightbranchLLStage3->getLevel() >= 7 && $rightbranchLRStage3->getLevel() >= 7 && $rightbranchRLStage3->getLevel() >= 7 && $rightbranchRRStage3->getLevel() >= 7) {
                    $branch->setLevel(8);
                    $em->persist($branch);
                }
            }

            if ($leftbranchStage1 != null && $rightbranchStage1 != null && $leftbranchLStage2 != null && $leftbranchRStage2 != null && $rightbranchLStage2 != null && $rightbranchRStage2 != null && $leftbranchLLStage3!= null && $leftbranchLRStage3!= null && $leftbranchRLStage3!= null && $leftbranchRRStage3!= null && $rightbranchLLStage3!= null && $rightbranchLRStage3!= null && $rightbranchRLStage3!= null && $rightbranchRRStage3!= null) {
//            Check level
                if ($leftbranchStage1->getLevel() >=8 && $rightbranchStage1->getLevel() >= 8 && $leftbranchLStage2->getLevel() >= 8 && $leftbranchRStage2->getLevel() >= 8 && $rightbranchLStage2->getLevel() == 8 && $rightbranchRStage2->getLevel() >= 8 && $leftbranchLRStage3->getLevel() >= 8 && $leftbranchRLStage3->getLevel() >= 8 && $leftbranchRRStage3->getLevel() >= 8 && $rightbranchLLStage3->getLevel() >= 8 && $rightbranchLRStage3->getLevel() >= 8 && $rightbranchRLStage3->getLevel() >= 8 && $rightbranchRRStage3->getLevel() >= 8) {
                    $branch->setLevel('INFINITY');
                    $em->persist($branch);
                }
            }

            $em->flush();
        }
    }

    public function calcCommission($user)
    {
        $em = $this->container->get('doctrine')->getManager();
//        $user=$this->getUser();
        $checkTransaction = $em->getRepository(Transaction::class)->findOneBy([
            'sender' => $user
        ]);
        $branch = $em->getRepository(Branches::class)->findOneBy(array(
            'userid' => $user->getClientcode()
        ));
        $clientCommmission = $em->getRepository(ClientCommission::class)->findOneBy([
            'client' => $user->getClient()
        ]);
        if ($branch != null) {
            if ($branch->getLevel() === 'Newbie member' && $clientCommmission->getLevelCommission() < 10) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 10);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(10);
                }
                $clientCommmission->setLevelCommission(10);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Senior member' && $clientCommmission->getLevelCommission() < 30) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 20);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(30);
                }
                $clientCommmission->setLevelCommission(30);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Bronze member' && $clientCommmission->getLevelCommission() < 70) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 40);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(70);
                }
                $clientCommmission->setLevelCommission(70);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Silver member' && $clientCommmission->getLevelCommission() < 370) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 300);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(370);
                }
                $clientCommmission->setLevelCommission(370);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Gold member' && $clientCommmission->getLevelCommission() < 1370) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 1000);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(1370);
                }
                $clientCommmission->setLevelCommission(1370);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Platinium member' && $clientCommmission->getLevelCommission() < 2370) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 2000);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(2370);
                }
                $clientCommmission->setLevelCommission(2370);
                $clientCommmission->setLevelLogistic(60);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Diamond 1 member' && $clientCommmission->getLevelCommission() < 3370) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 3000);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(3370);
                }
                $clientCommmission->setLevelCommission(3370);
                $clientCommmission->setLevelLogistic(100);
                $clientCommmission->setLevelMerit(1000);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Diamond 2 member' && $clientCommmission->getLevelCommission() < 4370) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 4000);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(4370);
                }
                $clientCommmission->setLevelCommission(4370);
                $clientCommmission->setLevelLogistic(200);
                $clientCommmission->setLevelMerit(2000);
                $em->persist($clientCommmission);
                $em->flush();

            }
            if ($branch->getLevel() === 'Diamond 3 member' && $clientCommmission->getLevelCommission() < 14370) {
                $clientCommmission->setBalance($clientCommmission->getBalance() + 14000);
                if ($checkTransaction == null) {
                    $clientCommmission->setBalance(14370);
                }
                $clientCommmission->setLevelCommission(14370);
                $clientCommmission->setLevelLogistic(300);
                $clientCommmission->setLevelMerit(3000);
                $em->persist($clientCommmission);
                $em->flush();

            }
            $balance = $clientCommmission->getBalance();
        } else {
            $balance = '0.00';
        }


        return new Response($balance . '$');
    }
}