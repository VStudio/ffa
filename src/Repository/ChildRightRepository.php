<?php

namespace App\Repository;

use App\Entity\ChildRight;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ChildRight|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChildRight|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChildRight[]    findAll()
 * @method ChildRight[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChildRightRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChildRight::class);
    }

    // /**
    //  * @return ChildRight[] Returns an array of ChildRight objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChildRight
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
