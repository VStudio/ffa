<?php

namespace App\Repository;

use App\Entity\ChildLeft;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ChildLeft|null find($id, $lockMode = null, $lockVersion = null)
 * @method ChildLeft|null findOneBy(array $criteria, array $orderBy = null)
 * @method ChildLeft[]    findAll()
 * @method ChildLeft[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChildLeftRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ChildLeft::class);
    }

    // /**
    //  * @return ChildLeft[] Returns an array of ChildLeft objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ChildLeft
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
