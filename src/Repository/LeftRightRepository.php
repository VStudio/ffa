<?php

namespace App\Repository;

use App\Entity\LeftRight;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method LeftRight|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeftRight|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeftRight[]    findAll()
 * @method LeftRight[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeftRightRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LeftRight::class);
    }

    // /**
    //  * @return LeftRight[] Returns an array of LeftRight objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeftRight
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
