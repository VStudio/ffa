<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LeftRightRepository")
 */
class LeftRight
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChildLeft", mappedBy="parent")
     */
    private $childLefts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChildRight", mappedBy="parent")
     */
    private $childRights;

    public function __construct()
    {
        $this->childLefts = new ArrayCollection();
        $this->childRights = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|ChildLeft[]
     */
    public function getChildLefts(): Collection
    {
        return $this->childLefts;
    }

    public function addChildLeft(ChildLeft $childLeft): self
    {
        if (!$this->childLefts->contains($childLeft)) {
            $this->childLefts[] = $childLeft;
            $childLeft->setParent($this);
        }

        return $this;
    }

    public function removeChildLeft(ChildLeft $childLeft): self
    {
        if ($this->childLefts->contains($childLeft)) {
            $this->childLefts->removeElement($childLeft);
            // set the owning side to null (unless already changed)
            if ($childLeft->getParent() === $this) {
                $childLeft->setParent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ChildRight[]
     */
    public function getChildRights(): Collection
    {
        return $this->childRights;
    }

    public function addChildRight(ChildRight $childRight): self
    {
        if (!$this->childRights->contains($childRight)) {
            $this->childRights[] = $childRight;
            $childRight->setParent($this);
        }

        return $this;
    }

    public function removeChildRight(ChildRight $childRight): self
    {
        if ($this->childRights->contains($childRight)) {
            $this->childRights->removeElement($childRight);
            // set the owning side to null (unless already changed)
            if ($childRight->getParent() === $this) {
                $childRight->setParent(null);
            }
        }

        return $this;
    }
}
