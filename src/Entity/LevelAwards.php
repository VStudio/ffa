<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LevelAwards
 *
 * @ORM\Table(name="level_awards")
 * @ORM\Entity(repositoryClass="App\Repository\LevelAwardsRepository")
 */
class LevelAwards
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="award", type="string", length=255)
     */
    private $award;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setAward(string $award): self
    {
        $this->award = $award;

        return $this;
    }

    public function getAward(): ?string
    {
        return $this->award;
    }
}
