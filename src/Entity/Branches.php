<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * branches
 *
 * @ORM\Table(name="branches")
 * @ORM\Entity(repositoryClass="App\Repository\BranchesRepository")
 */
class Branches
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientCode",)
     * @ORM\JoinColumn(name="userid",referencedColumnName="id",nullable=false)
     *
     */
    private $userid;

    /**
     * @var string
     *
     * @ORM\Column(name="names", type="string", length=255)
     */
    private $names;

    /**
     * @var string
     *
     * @ORM\Column(name="joinside", type="string", length=255, nullable=true)
     */
    private $joinside;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientCode",)
     * @ORM\JoinColumn(name="rBranch",referencedColumnName="id",nullable=true)
     *
     */
    private $rBranch;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\ClientCode",)
     * @ORM\JoinColumn(name="lBranch",referencedColumnName="id",nullable=true)
     *
     */
    private $lBranch;

    /**
     * @var int
     *
     * @ORM\Column(name="leftTotal", type="integer", nullable=true)
     */
    private $leftTotal;

    /**
     * @var int
     *
     * @ORM\Column(name="rightTotal", type="integer", nullable=true)
     */
    private $rightTotal;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string")
     */
    private $level;
    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", nullable=true)
     */
    private $ref;
    /**
     * @var int
     *
     * @ORM\Column(name="follwers", type="integer")
     */
    private $follwers;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;
    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNames(): ?string
    {
        return $this->names;
    }

    public function setNames(string $names): self
    {
        $this->names = $names;

        return $this;
    }

    public function getJoinside(): ?string
    {
        return $this->joinside;
    }

    public function setJoinside(string $joinside): self
    {
        $this->joinside = $joinside;

        return $this;
    }

    public function getLeftTotal(): ?int
    {
        return $this->leftTotal;
    }

    public function setLeftTotal(?int $leftTotal): self
    {
        $this->leftTotal = $leftTotal;

        return $this;
    }

    public function getRightTotal(): ?int
    {
        return $this->rightTotal;
    }

    public function setRightTotal(?int $rightTotal): self
    {
        $this->rightTotal = $rightTotal;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }

    public function getFollwers(): ?int
    {
        return $this->follwers;
    }

    public function setFollwers(int $follwers): self
    {
        $this->follwers = $follwers;

        return $this;
    }

    public function getUserid(): ?ClientCode
    {
        return $this->userid;
    }

    public function setUserid(?ClientCode $userid): self
    {
        $this->userid = $userid;

        return $this;
    }

    public function getRBranch(): ?ClientCode
    {
        return $this->rBranch;
    }

    public function setRBranch(?ClientCode $rBranch): self
    {
        $this->rBranch = $rBranch;

        return $this;
    }

    public function getLBranch(): ?ClientCode
    {
        return $this->lBranch;
    }

    public function setLBranch(?ClientCode $lBranch): self
    {
        $this->lBranch = $lBranch;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

}
