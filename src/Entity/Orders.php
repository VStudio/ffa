<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="App\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User",)
     * @ORM\JoinColumn(name="user",referencedColumnName="id",nullable=true)
     *
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="ordernumber", type="string", length=100)
     */
    private $ordernumber;

    /**
     * @var string
     *
     * @ORM\Column(name="productnumber", type="string", length=12, nullable=true)
     */
    private $productnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User",)
     * @ORM\JoinColumn(name="registeredUser",referencedColumnName="id",nullable=false)
     *
     */
    private $registeredUser;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;
    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setOrdernumber(string $ordernumber): self
    {
        $this->ordernumber = $ordernumber;

        return $this;
    }

    public function getOrdernumber(): ?string
    {
        return $this->ordernumber;
    }

    public function setProductnumber(?string $productnumber): self
    {
        $this->productnumber = $productnumber;

        return $this;
    }

    public function getProductnumber(): ?string
    {
        return $this->productnumber;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setRegisteredUser(?User $registeredUser): self
    {
        $this->registeredUser = $registeredUser;

        return $this;
    }

    public function getRegisteredUser(): ?User
    {
        return $this->registeredUser;
    }
}
