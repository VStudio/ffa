<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Editables
 *
 * @ORM\Table(name="editables")
 * @ORM\Entity(repositoryClass="App\Repository\EditablesRepository")
 */
class Editables
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="editField", type="text", nullable=true)
     */
    private $editField;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="lastModified", type="datetime", nullable=true)
     */
    private $lastModified;

  
    /**
     * @var int|null
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User",)
     * @ORM\JoinColumn(name="modifiedBy",referencedColumnName="id",nullable=true)
     *
     */
    private $modifiedBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setEditField(?string $editField): self
    {
        $this->editField = $editField;

        return $this;
    }

    public function getEditField(): ?string
    {
        return $this->editField;
    }

    public function setLastModified(?\DateTimeInterface $lastModified): self
    {
        $this->lastModified = $lastModified;

        return $this;
    }

    public function getLastModified(): ?\DateTimeInterface
    {
        return $this->lastModified;
    }

    public function setModifiedBy(?User $modifiedBy): self
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    public function getModifiedBy(): ?User
    {
        return $this->modifiedBy;
    }
}
