<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChildRightRepository")
 */
class ChildRight
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $rightleft;

    /**
     * @ORM\Column(type="integer")
     */
    private $rightright;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LeftRight", inversedBy="childRights")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parent;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ref;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRightleft(): ?int
    {
        return $this->rightleft;
    }

    public function setRightleft(int $rightleft): self
    {
        $this->rightleft = $rightleft;

        return $this;
    }

    public function getRightright(): ?int
    {
        return $this->rightright;
    }

    public function setRightright(int $rightright): self
    {
        $this->rightright = $rightright;

        return $this;
    }

    public function getParent(): ?LeftRight
    {
        return $this->parent;
    }

    public function setParent(?LeftRight $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }
}
