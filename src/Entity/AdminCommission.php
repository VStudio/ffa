<?php

namespace App\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdminCommission
 *
 * @ORM\Table(name="admin_commission")
 * @ORM\Entity(repositoryClass="App\Repository\AdminCommissionRepository")
 */
class AdminCommission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User",)
     * @ORM\JoinColumn(name="admin",referencedColumnName="id",nullable=true)
     *
     */
    private $admin;

    /**
     * @var int
     *
     * @ORM\Column(name="balance", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $balance;

    /**
     * @var int
     *
     * @ORM\Column(name="regbalance", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $regbalance;
    /**
     * @var int
     *
     * @ORM\Column(name="salemerite", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $salemerite;

    /**
     * @var string
     *
     * @ORM\Column(name="groupBonus", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $groupBonus;

    /**
     * @var string
     *
     * @ORM\Column(name="levelBonus", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $levelBonus;
    /**
     * @var string
     *
     * @ORM\Column(name="startupbonus", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $startupbonus;

    /**
     * @var string
     *
     * @ORM\Column(name="bottles", type="integer", nullable=true)
     */
    private $bottles;

    /**
     * @var string
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setBalance(?string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getRegbalance(): ?string
    {
        return $this->regbalance;
    }

    public function setRegbalance(?string $regbalance): self
    {
        $this->regbalance = $regbalance;

        return $this;
    }

    public function getSalemerite(): ?string
    {
        return $this->salemerite;
    }

    public function setSalemerite(?string $salemerite): self
    {
        $this->salemerite = $salemerite;

        return $this;
    }

    public function getGroupBonus(): ?string
    {
        return $this->groupBonus;
    }

    public function setGroupBonus(?string $groupBonus): self
    {
        $this->groupBonus = $groupBonus;

        return $this;
    }

    public function getLevelBonus(): ?string
    {
        return $this->levelBonus;
    }

    public function setLevelBonus(?string $levelBonus): self
    {
        $this->levelBonus = $levelBonus;

        return $this;
    }

    public function getStartupbonus(): ?string
    {
        return $this->startupbonus;
    }

    public function setStartupbonus(?string $startupbonus): self
    {
        $this->startupbonus = $startupbonus;

        return $this;
    }

    public function getBottles(): ?int
    {
        return $this->bottles;
    }

    public function setBottles(?int $bottles): self
    {
        $this->bottles = $bottles;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getAdmin(): ?User
    {
        return $this->admin;
    }

    public function setAdmin(?User $admin): self
    {
        $this->admin = $admin;

        return $this;
    }
}
