<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Weekly
 *
 * @ORM\Table(name="weekly")
 * @ORM\Entity(repositoryClass="App\Repository\WeeklyRepository")
 */
class Weekly
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="date")
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="date")
     */
    private $endDate;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=100)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(name="globalPv", type="integer", nullable=true)
     */
    private $globalPv;

    /**
     * @var int
     *
     * @ORM\Column(name="globalbalance", type="integer", nullable=true)
     */
    private $globalbalance;

    /**
     * @var int
     *
     * @ORM\Column(name="globallevelMerit", type="integer", nullable=true)
     */
    private $globallevelMerit;

    /**
     * @var int
     *
     * @ORM\Column(name="globallevelLogistic", type="integer", nullable=true)
     */
    private $globallevelLogistic;

    /**
     * @var int
     *
     * @ORM\Column(name="globalgroupBonus", type="integer", nullable=true)
     */
    private $globalgroupBonus;

    /**
     * @var int
     *
     * @ORM\Column(name="globalregbalance", type="integer", nullable=true)
     */
    private $globalregbalance;

    /**
     * @var int
     *
     * @ORM\Column(name="globalsalemerite", type="integer", nullable=true)
     */
    private $globalsalemerite;

    /**
     * @var int
     *
     * @ORM\Column(name="globalsalelogistics", type="integer", nullable=true)
     */
    private $globalsalelogistics;

    /**
     * @var int
     *
     * @ORM\Column(name="globalstartupbonus", type="integer", nullable=true)
     */
    private $globalstartupbonus;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setGlobalPv(?int $globalPv): self
    {
        $this->globalPv = $globalPv;

        return $this;
    }

    public function getGlobalPv(): ?int
    {
        return $this->globalPv;
    }

    public function setGlobalbalance(?int $globalbalance): self
    {
        $this->globalbalance = $globalbalance;

        return $this;
    }

    public function getGlobalbalance(): ?int
    {
        return $this->globalbalance;
    }

    public function setGloballevelMerit(?int $globallevelMerit): self
    {
        $this->globallevelMerit = $globallevelMerit;

        return $this;
    }

    public function getGloballevelMerit(): ?int
    {
        return $this->globallevelMerit;
    }

    public function setGloballevelLogistic(?int $globallevelLogistic): self
    {
        $this->globallevelLogistic = $globallevelLogistic;

        return $this;
    }

    public function getGloballevelLogistic(): ?int
    {
        return $this->globallevelLogistic;
    }

    public function setGlobalgroupBonus(?int $globalgroupBonus): self
    {
        $this->globalgroupBonus = $globalgroupBonus;

        return $this;
    }

    public function getGlobalgroupBonus(): ?int
    {
        return $this->globalgroupBonus;
    }

    public function setGlobalregbalance(?int $globalregbalance): self
    {
        $this->globalregbalance = $globalregbalance;

        return $this;
    }

    public function getGlobalregbalance(): ?int
    {
        return $this->globalregbalance;
    }

    public function setGlobalsalemerite(?int $globalsalemerite): self
    {
        $this->globalsalemerite = $globalsalemerite;

        return $this;
    }

    public function getGlobalsalemerite(): ?int
    {
        return $this->globalsalemerite;
    }

    public function setGlobalsalelogistics(?int $globalsalelogistics): self
    {
        $this->globalsalelogistics = $globalsalelogistics;

        return $this;
    }

    public function getGlobalsalelogistics(): ?int
    {
        return $this->globalsalelogistics;
    }

    public function setGlobalstartupbonus(?int $globalstartupbonus): self
    {
        $this->globalstartupbonus = $globalstartupbonus;

        return $this;
    }

    public function getGlobalstartupbonus(): ?int
    {
        return $this->globalstartupbonus;
    }
}
