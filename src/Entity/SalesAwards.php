<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SalesAwards
 *
 * @ORM\Table(name="sales_awards")
 * @ORM\Entity(repositoryClass="App\Repository\SalesAwardsRepository")
 */
class SalesAwards
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="awards", type="string", length=255)
     */
    private $awards;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setLevel(string $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getLevel(): ?string
    {
        return $this->level;
    }

    public function setAwards(string $awards): self
    {
        $this->awards = $awards;

        return $this;
    }

    public function getAwards(): ?string
    {
        return $this->awards;
    }
}
