<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChildLeftRepository")
 */
class ChildLeft
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $leftleft;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $leftright;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LeftRight", inversedBy="childLefts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parent;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $ref;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLeftleft(): ?int
    {
        return $this->leftleft;
    }

    public function setLeftleft(?int $leftleft): self
    {
        $this->leftleft = $leftleft;

        return $this;
    }

    public function getLeftright(): ?int
    {
        return $this->leftright;
    }

    public function setLeftright(?int $leftright): self
    {
        $this->leftright = $leftright;

        return $this;
    }

    public function getParent(): ?LeftRight
    {
        return $this->parent;
    }

    public function setParent(?LeftRight $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getRef(): ?string
    {
        return $this->ref;
    }

    public function setRef(string $ref): self
    {
        $this->ref = $ref;

        return $this;
    }
}
