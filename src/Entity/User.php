<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="App\Entity\ClientCode",)
     * @ORM\JoinColumn(name="clientcode",referencedColumnName="id",nullable=true)
     *
     */
    private $clientcode;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Clients",)
     * @ORM\JoinColumn(name="client",referencedColumnName="id",nullable=true)
     *
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setClientcode(?ClientCode $clientcode): self
    {
        $this->clientcode = $clientcode;

        return $this;
    }

    public function getClientcode(): ?ClientCode
    {
        return $this->clientcode;
    }

    public function setClient(?Clients $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getClient(): ?Clients
    {
        return $this->client;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
