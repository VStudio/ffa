<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ClientCommission
 *
 * @ORM\Table(name="client_commission")
 * @ORM\Entity(repositoryClass="App\Repository\ClientCommissionRepository")
 */
class ClientCommission
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="levelCommission", type="decimal", precision=10, scale=2)
     */
    private $levelCommission;

    /**
     * @var int
     *
     * @ORM\Column(name="bottles", type="integer", nullable=true)
     */
    private $bottles;

    /**
     * @var boolean
     *
     * @ORM\Column(name="groupBonusAllowed", type="boolean", nullable=true)
     */
    private $groupBonusAllowed=0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="salesBonusAllowed", type="boolean", nullable=true)
     */
    private $salesBonusAllowed=1;

    /**
     * @var double
     *
     * @ORM\Column(name="balance", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $balance;

    /**
     * @var int
     *
     * @ORM\Column(name="levelMerit", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $levelMerit;

    /**
     * @var string
     *
     * @ORM\Column(name="levelaward", type="string", nullable=true)
     */
    private $levelaward;

    /**
     * @var int
     *
     * @ORM\Column(name="levelLogistic", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $levelLogistic;

    /**
     * @var string
     *
     * @ORM\Column(name="groupBonus", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $groupBonus;

    /**
     * @var int
     *
     * @ORM\Column(name="regbalance", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $regbalance;

    /**
     * @var string
     *
     * @ORM\Column(name="salemerite", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $salemerite;

    /**
     * @var string
     *
     * @ORM\Column(name="salelogistics", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $salelogistics=0;

    /**
     * @var string
     *
     * @ORM\Column(name="salesaward", type="string", nullable=true)
     */
    private $salesaward;

    /**
     * @var string
     *
     * @ORM\Column(name="pv", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $pv=0;

    /**
     * @var string
     *
     * @ORM\Column(name="mypv", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $mypv=0;

    /**
     * @var string
     *
     * @ORM\Column(name="newpv", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $newpv=0;
  
    /**
     * @var string
     *
     * @ORM\Column(name="tempPv", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $tempPv=0;
   
    /**
     * @var string
     *
     * @ORM\Column(name="startupbonus", type="decimal", precision=11, scale=2, nullable=true)
     */
    private $startupbonus=0;

    /**
     * @var string
     *
     * @ORM\OneToOne(targetEntity="App\Entity\Clients",)
     * @ORM\JoinColumn(name="client",referencedColumnName="id",nullable=true)
     *
     */
    private $client;

    /**
     * @var string
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;
    public function __construct()
    {
        $this->created = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setLevelCommission(string $levelCommission): self
    {
        $this->levelCommission = $levelCommission;

        return $this;
    }

    public function getLevelCommission(): ?string
    {
        return $this->levelCommission;
    }

    public function setBottles(?int $bottles): self
    {
        $this->bottles = $bottles;

        return $this;
    }

    public function getBottles(): ?int
    {
        return $this->bottles;
    }

    public function setGroupBonusAllowed(?bool $groupBonusAllowed): self
    {
        $this->groupBonusAllowed = $groupBonusAllowed;

        return $this;
    }

    public function getGroupBonusAllowed(): ?bool
    {
        return $this->groupBonusAllowed;
    }

    public function setSalesBonusAllowed(?bool $salesBonusAllowed): self
    {
        $this->salesBonusAllowed = $salesBonusAllowed;

        return $this;
    }

    public function getSalesBonusAllowed(): ?bool
    {
        return $this->salesBonusAllowed;
    }

    public function setBalance(?string $balance): self
    {
        $this->balance = $balance;

        return $this;
    }

    public function getBalance(): ?string
    {
        return $this->balance;
    }

    public function setLevelMerit(?string $levelMerit): self
    {
        $this->levelMerit = $levelMerit;

        return $this;
    }

    public function getLevelMerit(): ?string
    {
        return $this->levelMerit;
    }

    public function setLevelaward(?string $levelaward): self
    {
        $this->levelaward = $levelaward;

        return $this;
    }

    public function getLevelaward(): ?string
    {
        return $this->levelaward;
    }

    public function setLevelLogistic(?string $levelLogistic): self
    {
        $this->levelLogistic = $levelLogistic;

        return $this;
    }

    public function getLevelLogistic(): ?string
    {
        return $this->levelLogistic;
    }

    public function setGroupBonus(?string $groupBonus): self
    {
        $this->groupBonus = $groupBonus;

        return $this;
    }

    public function getGroupBonus(): ?string
    {
        return $this->groupBonus;
    }

    public function setRegbalance(?string $regbalance): self
    {
        $this->regbalance = $regbalance;

        return $this;
    }

    public function getRegbalance(): ?string
    {
        return $this->regbalance;
    }

    public function setSalemerite(?string $salemerite): self
    {
        $this->salemerite = $salemerite;

        return $this;
    }

    public function getSalemerite(): ?string
    {
        return $this->salemerite;
    }

    public function setSalelogistics(?string $salelogistics): self
    {
        $this->salelogistics = $salelogistics;

        return $this;
    }

    public function getSalelogistics(): ?string
    {
        return $this->salelogistics;
    }

    public function setSalesaward(?string $salesaward): self
    {
        $this->salesaward = $salesaward;

        return $this;
    }

    public function getSalesaward(): ?string
    {
        return $this->salesaward;
    }

    public function setPv(?string $pv): self
    {
        $this->pv = $pv;

        return $this;
    }

    public function getPv(): ?string
    {
        return $this->pv;
    }

    public function setMypv(?string $mypv): self
    {
        $this->mypv = $mypv;

        return $this;
    }

    public function getMypv(): ?string
    {
        return $this->mypv;
    }

    public function setNewpv(?string $newpv): self
    {
        $this->newpv = $newpv;

        return $this;
    }

    public function getNewpv(): ?string
    {
        return $this->newpv;
    }

    public function setStartupbonus(?string $startupbonus): self
    {
        $this->startupbonus = $startupbonus;

        return $this;
    }

    public function getStartupbonus(): ?string
    {
        return $this->startupbonus;
    }

    public function setCreated(?\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setClient(?Clients $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getClient(): ?Clients
    {
        return $this->client;
    }

    public function setTempPv(?string $tempPv): self
    {
        $this->tempPv = $tempPv;

        return $this;
    }

    public function getTempPv(): ?string
    {
        return $this->tempPv;
    }
}
