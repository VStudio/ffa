var firstname = $('#clients_firstname');
var lastname = $('#clients_lastname');
var email = $('#clients_email');
var telephone = $('#clients_telephone');
var transac = $('#clients_transac');

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

$('#checker').click(function () {
    var formComplete = true;
    if (firstname.val() === "") {
        swal("Error First name", "Please kindly enter the members firstname", 'warning');
        formComplete = false;
    }
    if (lastname.val() === "") {
        swal("Error Last name", "Please kindly enter the members lastname", 'warning');
        formComplete = false;
    }
    if (email.val() === "") {
        swal("Error Email", "Please kindly enter the members email", 'warning');
        formComplete = false;
    }
    console.log(isValidEmailAddress(email.val()));
    if (!isValidEmailAddress(email.val())) {
        swal("Error Email", "Please kindly enter valid email", 'warning');
        formComplete = false;
    }
    if (telephone.val() === "") {
        swal("Error Telephone", "Please kindly enter the members telephone", 'warning');
        formComplete = false;
    }
    if (transac.val() === "") {
        swal("Error Transaction", "Please kindly enter your transaction password", 'danger');
        formComplete = false;
    }
    if (formComplete === true) {
        swal({
            title: "Are you sure?",
            text: "10$ will be deducted from your registration unit!",
            icon: "warning",
            // confirmButtonColor: "#00c7e8",
            buttons: {
                cancel: {
                    text: "No, abort!",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: false,
                },
                confirm: {
                    text: "Yes, create member!",
                    value: true,
                    visible: true,
                    className: "",
                    closeModal: false
                }
            }
        })
            .then((isConfirm) => {
                if (isConfirm) {
                    swal("Ok!", "Please do turn off your internet or cancel the creation process.", "success");
                    $("#submitBtn").trigger("click");
                } else {
                    swal("Cancelled", "Operation cancelled", "error");
                }
            });

        // $('#submitter').trigger('click');
    }
});